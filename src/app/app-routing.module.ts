import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
	{ path: "", redirectTo: "/home", pathMatch: "full" },
	{ path: "home", loadChildren: "~/app/home/home.module#HomeModule" },
	{ path: "crm", loadChildren: "~/app/crm/crm.module#CrmModule" },
	{ path: "scanned-disc", loadChildren: "~/app/scanned-disc/scanned-disc.module#ScannedDiscModule"},
	{ path: "support", loadChildren: "~/app/support/support.module#SupportModule" },
	{ path: "settings", loadChildren: "~/app/settings/settings.module#SettingsModule" },
	{ path: "showroom", loadChildren: "~/app/showroom/cars.module#CarsModule" },
	{ path: "transunion-pricing", loadChildren: "~/app/transunion-pricing/transunion-pricing.module#TransunionPricingModule" }
];

@NgModule({
	imports: [NativeScriptRouterModule.forRoot(routes)],
	exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {
}
