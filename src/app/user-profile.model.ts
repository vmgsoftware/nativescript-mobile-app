export class UserProfileModel {
	public userId: string;
	public branchGuid: string;
	public companyName: string;
	public companyId: number;
	public isLockedOut: boolean;
	public firstName: string;
	public lastName: string;
	public emailAddress: string;
	public contactNumber: string;
	public userSecurityRole: UserSecurityRole;
}

export class UserSecurityRole{
	public canViewStock: boolean;
	public canAddEditStock: boolean;
	public canViewPurchasePrice: boolean;
	public canViewLeads: boolean;
	public canAddEditLeads: boolean;
	public canDeleteLeads: boolean;
	public canScanDisks: boolean;
	public canUseTransunion: boolean;
}
