import { Component, ElementRef, OnInit, Renderer2, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { LoadingIndicator } from "@nstudio/nativescript-loading-indicator";
import * as application from "application";
import { RouterExtensions } from "nativescript-angular";
import * as appSettings from "tns-core-modules/application-settings";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import { action, ActionOptions, alert } from "tns-core-modules/ui/dialogs";
import { Color } from "tns-core-modules/ui/frame";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import * as dialogs from "ui/dialogs";
import { Page } from "ui/page";
import { TextField } from "ui/text-field";
import { Config } from "~/app/Config";
import { LeadService } from "~/app/crm/shared/lead.service";
import { CarService } from "~/app/DataLayer/car.service";
import { Car } from "~/app/DataLayer/Entities/Car";
import { Lead } from "~/app/DataLayer/Entities/Lead";
import { MmcodesRepository } from "~/app/DataLayer/repositories/mmcodes.repository";
import { StringHelper } from "~/app/shared/helpers/string-helper";
import { NotificationService } from "~/app/shared/notification.service";
import { SharedService } from "~/app/shared/shared.service";

const regionsList = ["Eastern Cape", "Free State", "Gauteng", "KwaZulu-Natal", "Limpopo",
	"Mpumalanga", "Northern Cape", "North West", "Western Cape"];

@Component({
	selector: "ns-add-lead",
	templateUrl: "./add-lead.component.html",
	styleUrls: ["./add-lead.component.scss"],
	moduleId: module.id
})
export class AddLeadComponent implements OnInit {

	get isLoading(): boolean {
		return this._isLoading;
	}

	@ViewChild("vehicleDetailsFilter", {static: false}) public vehicleDetailsFilter: ElementRef;
	@ViewChild("modelDetailsFilter", {static: false}) public modelDetailsFilter: ElementRef;

	public listItems: any = [];

	public indicator = new LoadingIndicator();
	public firstname: string = "";
	public lastname: string = "";
	public emailAddress: string = "";
	public cellphoneNumber: string = "";
	public addressLine1: string = "";
	public addressLine2: string = "";
	public addressLine3: string = "";
	public region: string = "";
	public category: string = "";
	public make: string = "";
	public model: string = "";
	public variant: string = "";
	public year: number = null;
	public price: number = null;
	public colour: string = "";
	public direction: number;
	public tabSelectedIndex: number;
	public regions: Array<string>;
	public years: Array<string>;
	public selectedIndexYear: number;
	public platformIOS = isIOS;
	public platformAndroid = isAndroid;

	public vehicleCategory: Array<string>;
	public makes: Array<string>;
	public models: Array<string>;
	public variants: Array<string>;
	public car: Car;

	private selectedFilter: string = "";
	private _isLoading: boolean = true;
	private validationMessage: string = "";

	constructor(private _leadService: LeadService,
				         private  _routerExtensions: RouterExtensions,
				         private  renderer: Renderer2,
				         private _carService: CarService,
				         private _mmCodesRepo: MmcodesRepository,
				         private page: Page,
				         private _sharedService: SharedService,
				         private _notificationService: NotificationService,
				         private _activatedRoute: ActivatedRoute) {
		if (isAndroid) {
			this.page.on(application.AndroidApplication.activityBackPressedEvent, this.onBackButtonTap, this);
		}

		this.car = this._carService.selectedCar ? this._carService.selectedCar : new Car();

		this.setVehicleDetails();

		// _activatedRoute.queryParams.subscribe((params) => {
		// 	this.car.id = params["carId"] || "";
		// });
		//
		// console.log(this.car.id);

		this.tabSelectedIndex = 0;
		this.regions = [];
		this.tabSelectedIndex = 0;
		this.selectedIndexYear = 0;
		const curYear = new Date();
		const curYearNum = curYear.getFullYear();
		for (const region of regionsList) {
			this.regions.push(region);
		}
		this.years = [];
		for (let i = 0; i < 100; i++) {
			this.years.push((curYearNum - i).toString());
		}
	}

	public setVehicleDetails(): void {
		if (!StringHelper.isNullOrEmpty(this.car.category)) {
			this.category = this.car.category;
		}

		if (!StringHelper.isNullOrEmpty(this.car.make)) {
			this.make = this.car.make;
		}

		if (!StringHelper.isNullOrEmpty(this.car.model)) {
			this.model = this.car.model;
		}

		if (!StringHelper.isNullOrEmpty(this.car.variant)) {
			this.variant = this.car.variant;
		}

		if (!StringHelper.isNullOrEmpty(this.car.colour)) {
			this.colour = this.car.colour;
		}

		if (this.car.year) {
			this.year = this.car.year;
		}
		if (this.car.sellingPrice) {
			this.price = this.car.sellingPrice;
		}
	}

	public onSwipe(args: SwipeGestureEventData) {
		this.direction = args.direction;
		if (this.tabSelectedIndex !== 2 && this.direction === 2) {
			this.tabSelectedIndex = this.tabSelectedIndex + 1;
		} else if (this.tabSelectedIndex !== 0 && this.direction === 1) {
			this.tabSelectedIndex = this.tabSelectedIndex - 1;
		}

	}

	public async ngOnInit() {
		this._isLoading = true;
		await this.populateVehicleCategory().then(() => {
			this._isLoading = false;
		});
	}

	public async onLeadAdd() {
		if (this.formIsValid()) {
			let _lead = new Lead();
			_lead.firstname = this.firstname;
			_lead.lastname = this.lastname;
			_lead.emailAddress = this.emailAddress;
			_lead.cellphoneNumber = this.cellphoneNumber;
			_lead.addressLine1 = this.addressLine1;
			_lead.addressLine2 = this.addressLine2;
			_lead.addressLine3 = this.addressLine3;
			_lead.region = this.region;
			_lead.make = this.make;
			_lead.model = this.model;
			_lead.variant = this.variant;
			_lead.year = this.year;
			if (_lead.year === 0) {
				_lead.year = null;
			}
			_lead.price = this.price;
			_lead.colour = this.colour;
			_lead.userId = appSettings.getString("userId");
			_lead.branchGuid = appSettings.getString("CurrentBranchGuid");
			_lead.isOnline = false;
			const curDate = new Date();
			_lead.dateCreated = curDate.toString().substring(0, curDate.toString().length - 15);
			_lead.dateUpdated = curDate.toString().substring(0, curDate.toString().length - 15);
			_lead.dmsLeadId = null;
			_lead.id = null;
			_lead = await this._leadService.translateMakeModelvariantToMmcode(_lead);
			await this._leadService.AddLead(_lead).then(async () => {
				await this._notificationService.successToastMessage("Lead Added", `${_lead.firstname} ${_lead.lastname} added successfully`);
			});
			await this._leadService.GetLeads();
			await this._routerExtensions.navigate(["/crm"], {
				transition: {
					name: "fade"
				}
			});
		} else {
			await this._notificationService.warningToastMessage("Required Fields", this.validationMessage, 5000);
		}
	}

	public async changeCategory() {
		this.selectedFilter = "Category";
		this.listItems = this.vehicleCategory;
		this.vehicleDetailsFilter.nativeElement.show();
	}

	public async changeMake() {
		if (this.category !== "") {
			if (this.makes.length > 0) {
				this.selectedFilter = "Make";
				this.listItems = this.makes;
				this.vehicleDetailsFilter.nativeElement.show();
			} else {
				await this._notificationService.infoToastMessage(`Vehicle category ${this.category} has no makes`, "");
			}
		} else {
			await this._notificationService.warningToastMessage("Selection Required", "Please select a Category.");
		}
	}

	public async changeModel() {
		if (this.make !== "") {
			if (this.models.length > 0) {
				this.selectedFilter = "Model Description";
				this.listItems = this.models;
				this.modelDetailsFilter.nativeElement.show();
			} else {
				await this._notificationService.infoToastMessage(`Vehicle make ${this.make} has no models`, "");
			}
		} else {
			await this._notificationService.warningToastMessage("Selection Required", "Please select a Make.");
		}
	}

	public async changeVariant() {
		if (this.model !== "") {
			if (this.variants.length > 0) {
				this.selectedFilter = "Model";
				this.listItems = this.variants;
				this.vehicleDetailsFilter.nativeElement.show();
			} else {
				await this._notificationService.infoToastMessage(`Vehicle Model ${this.model} has no variants`, "");
			}

		} else {
			await this._notificationService.warningToastMessage("Selection Required", "Please select a Model Description.");
		}
	}

	public async vehicleDetailsFilterSelected(args) {
		if (this.selectedFilter === "Region") {
			this.regionSelected(args);
		}

		if (this.selectedFilter === "Year") {
			this.yearSelected(args);
		}

		if (this.selectedFilter === "Category") {
			this.categorySelected(args);
		}

		if (this.selectedFilter === "Make") {
			this.makeSelected(args);
		}

		if (this.selectedFilter === "Model Description") {
			this.modelDescriptionSelected(args);
		}

		if (this.selectedFilter === "Model") {
			this.variantSelected(args);
		}

	}

	public async regionSelected(args) {
		const index = this.regions.indexOf(args.selectedItem);
		this.region = this.regions[index];
	}

	public async yearSelected(args) {
		const index = this.years.indexOf(args.selectedItem);
		this.year = Number(this.years[index]);
	}

	public async categorySelected(args) {
		const index = this.vehicleCategory.indexOf(args.selectedItem);
		// Reset form to default/empty
		this.categoryReselected();
		this.category = this.vehicleCategory[index];
		await this.populateMake();
	}

	public async makeSelected(args) {
		const index = this.makes.indexOf(args.selectedItem);
		// Reset form to default/empty
		this.makeReselected();
		this.make = this.makes[index];
		await this.populateModel();
	}

	public async modelDescriptionSelected(args) {
		const index = this.models.indexOf(args.selectedItem);
		// Reset form to default/empty
		this.modelReselected();
		this.model = this.models[index];
		await this.populateVariant();
	}

	public async variantSelected(args) {
		const index = this.variants.indexOf(args.selectedItem);
		this.variant = this.variants[index];
	}

	public async onBackButtonTap() {
		this.indicator.show(Config.loaderOptions);
		await this._routerExtensions.navigate(["/crm"], {
			transition: {
				name: "fade"
			}
		}).then(() => {
			setTimeout(async () => {
				this.indicator.hide();
			}, 500);
		});
	}

	public changeRegion() {
		this.selectedFilter = "Region";
		this.listItems = this.regions;
		this.vehicleDetailsFilter.nativeElement.show();
	}

	public changeYear() {
		this.selectedFilter = "Year";
		this.listItems = this.years;
		this.vehicleDetailsFilter.nativeElement.show();
	}

	public formIsValid(): boolean {
		this.validationMessage = "Please complete the following fields: \n\n";
		let valid: boolean = false;
		const regexpEmail = new RegExp("[\\w-]+@([\\w-]+\\.)+[\\w-]+");
		const validEmail = regexpEmail.test(this.emailAddress);
		const regexpPrice = new RegExp("^[0-9]*$");
		let priceToCheck = this.price;
		if (priceToCheck === null) {
			priceToCheck = 0;
		}
		const validPrice = regexpPrice.test(priceToCheck.toString());

		if (this.firstname === "") {
			this.validationMessage += "Firstname is required. \n";
		}

		if (this.cellphoneNumber === "") {
			this.validationMessage += "Cellphone number is required. \n";
		}

		if (this.cellphoneNumber.length < 10 && this.cellphoneNumber !== "") {
			this.validationMessage += "Cellphone number needs to have a minimum of 10 characters. \n";
		}

		if (this.emailAddress === "") {
			this.validationMessage += "Email address is required. \n";

		}

		if (!validEmail && this.emailAddress !== "") {
			this.validationMessage += "Email address is incorrect. \n";
		}

		if (this.firstname !== "" && this.cellphoneNumber.length >= 10 &&
			validEmail && validPrice) {
			valid = true;

			return valid;
		}

		return valid;
	}

	public clearVehicleDetails() {
		this.category = "";
		this.make = "";
		this.model = "";
		this.variant = "";
		this.year = null;
		this.price = null;
		this.colour = "";
	}

	private categoryReselected() {
		this.make = "";
		this.model = "";
		this.variant = "";
	}

	private makeReselected() {
		this.model = "";
		this.variant = "";
	}

	private modelReselected() {
		this.variant = "";
	}

	private async populateVehicleCategory() {
		this.vehicleCategory = this._sharedService.vehicleCategories;
	}

	private async populateMake() {
		this.makes = [];
		this.makes = await this._mmCodesRepo.getMakesByCategory(this.category);
	}

	private async populateModel() {
		this.models = [];
		this.models = await this._mmCodesRepo.getMasterModelsByMakeAndCategory(this.make, this.category);
	}

	private async populateVariant() {
		this.variants = [];
		this.variants = await this._mmCodesRepo.getVariantsByMasterModel(this.make, this.model);
	}
}
