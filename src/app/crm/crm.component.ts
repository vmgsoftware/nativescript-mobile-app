import { Component, NgZone, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { LoadingIndicator } from "@nstudio/nativescript-loading-indicator";
import * as application from "application";
import { registerElement } from "nativescript-angular";
import { RouterExtensions } from "nativescript-angular/router";
import * as appavailability from "nativescript-appavailability";
import * as email from "nativescript-email";
// import * as TNSPhone from "nativescript-phone";
import { ListViewEventData, LoadOnDemandListViewEventData, RadListView } from "nativescript-ui-listview";
import { RadListViewComponent } from "nativescript-ui-listview/angular";
import * as appSettings from "tns-core-modules/application-settings";
import { Color } from "tns-core-modules/color";
import { EventData, isAndroid, isIOS, View } from "tns-core-modules/ui/core/view";
import { alert } from "tns-core-modules/ui/dialogs";
import { Frame } from "tns-core-modules/ui/frame";
import { openUrl } from "tns-core-modules/utils/utils";
import { Like } from "typeorm/browser";
import { Page } from "ui/page";
import { SearchBar } from "ui/search-bar";
import { Config } from "~/app/Config";
import { LeadService } from "~/app/crm/shared/lead.service";
import { Lead } from "~/app/DataLayer/Entities/Lead";
import { LeadRepository } from "~/app/DataLayer/repositories/lead-repository";
import { EmailService } from "~/app/shared/email.service";
import { NotificationService } from "~/app/shared/notification.service";
import { SharedService } from "~/app/shared/shared.service";

@Component ({
	selector: "Crm",
	moduleId: module.id,
	templateUrl: "./crm.component.html",
 styleUrls: ["./crm.scss"]
})
export class CrmComponent implements OnInit {

	get isLoading(): boolean {
		return this._isLoading;
	}

	@ViewChild("myListView", {static: false}) public listViewComponent: RadListViewComponent;

	public composeOptions: email.ComposeOptions;
	public leadsOnView: Array<Lead> = new Array<Lead>();
	public platformIOS = isIOS;
	public platformAndroid = isAndroid;
	public totalLeads: any;
	public searchTerm: string = "";
	public loadingMode: string = "Auto";

	// public count: number = 0;
	// public searchCounter: number = 10;
	public numberOfSearchResults: number;
	public isSearching: boolean = false;
	public firstBatchOfLeads: Array<Lead>;
	public indicator = new LoadingIndicator();
	private _isLoading: boolean = false;

	constructor(private leadService: LeadService,
				         private activeRoute: ActivatedRoute,
				         private _routerExtensions: RouterExtensions,
				         private _emailService: EmailService, private page: Page, private ngZone: NgZone, private _sharedService: SharedService,
				         private _leadRepo: LeadRepository, private _notificationService: NotificationService) {

		if (isAndroid) {
			this.page.on(application.AndroidApplication.activityBackPressedEvent, this.onBackButtonTap, this);
		}

	}

	public async ngOnInit() {
		await this.setupLeadsPage();
	}

	public async refreshList(args: ListViewEventData) {
		if (!this.isSearching) {
			this.loadingMode = "Auto";
			const that = new WeakRef(this);

			setTimeout(async () => {
				that.get()._isLoading = true;
				await that.get().leadService.GetLeads();
				that.get().leadsOnView = [];
				that.get().firstBatchOfLeads = [];
				that.get().firstBatchOfLeads = await this._leadRepo.getAllPaged(0, 10 , { dateCreated: "DESC" });
				that.get().leadsOnView.push(...that.get().firstBatchOfLeads);
				that.get().totalLeads = await this._leadRepo.getTotal();

				that.get()._isLoading = false;

				const listView = args.object;
				try {
					listView.notifyPullToRefreshFinished();
					listView.notifyLoadOnDemandFinished();
				} catch (e) {
					console.log("pull to refresh error : ", e);
				}

			}, 1000);
		} else {
			setTimeout(async function() {
				const listView = args.object;
				listView.notifyPullToRefreshFinished();
				listView.notifyLoadOnDemandFinished();
			}, 1000);
		}

	}

	public onLeadsLoading(args: ListViewEventData) {
		if (args.index % 2 === 0) {
			args.view.backgroundColor = new Color("#eee");
		} else {
			args.view.backgroundColor = new Color("#e6e6e6");
		}
		const crmContent = args.view.bindingContext;

		const currentDate = new Date();
		const leadCreated = new Date(crmContent.dateCreated);
		const timelapse = currentDate.getDate() - leadCreated.getDate();
	}

	public onSwipeCellStarted(args: ListViewEventData) {
		const swipeLimits = args.data.swipeLimits;
		const swipeView = args.object;
		const rightItem = swipeView.getViewById<View>("share-view");
		swipeLimits.right = rightItem.getMeasuredWidth();
		swipeLimits.left = 0;
		swipeLimits.threshold = rightItem.getMeasuredWidth() / 2;
	}

	public onLeftSwipeClick(args: ListViewEventData) {
		const leftSwipe = args.view;
		const leftSwipeItem = leftSwipe.bindingContext;
		let phoneNumber = leftSwipeItem.cellphoneNumber;
		phoneNumber = phoneNumber.toString();
		var phone = require('nativescript-phone');
		phone.dial(phoneNumber, false);
		// TNSPhone.requestCallPermission ("You should accept the permission to be able to make a direct phone call.")
		// 	.then (() => TNSPhone.dial (phoneNumber, false))
		// 	.catch (() => TNSPhone.dial (phoneNumber, true));
	}

	public onRightSwipeClick(args) {
		const rightSwipe = args.view;
		const rightSwipeItem = rightSwipe.bindingContext;
		let phoneNumber = rightSwipeItem.cellphoneNumber;
		phoneNumber = phoneNumber.toString().substring(1, 10);
		const name = rightSwipeItem.firstname;
		if (phoneNumber != null) {
			if (Frame.topmost().ios) {
				appavailability.available("whatsapp://")
					.then((avail: boolean) => {
						if (avail) {
							const encodedURI = encodeURI("whatsapp://send?phone=27" + phoneNumber + "&text=Good day, " + name);
							openUrl(encodedURI);
						} else {
							console.log("Whatsapp is not installed");
						}
					});
			} else {
				appavailability.available("com.whatsapp")
					.then((avail: boolean) => {
						if (avail) {
							const encodedURI = encodeURI("https://api.whatsapp.com/send?phone=+27" + phoneNumber + "&text=Good day, " + name);
							openUrl(encodedURI);
						} else {
							console.log("Whatsapp is not installed");
						}
					});
			}
		}
	}

	public sendSms(args) {
		const rightSwipe = args.view;
		const rightSwipeItem = rightSwipe.bindingContext;
		let phoneNumber = rightSwipeItem.cellphoneNumber;
		phoneNumber = phoneNumber.toString();
		const name = rightSwipeItem.firstname;
		if (phoneNumber != null) {
			var phone = require('nativescript-phone');
		phone.sms([phoneNumber], "Good day, ").then((result) => {
			console.dir(result);
		}, (error) => {
			console.dir(error);
		});
		
	}
	}

	public async sendEmail(args: ListViewEventData) {
		const crmContent = args.view.bindingContext;
		const leadMail = crmContent.emailAddress;

		await this._emailService.sendEmail(leadMail);

	}

	public async onFilterTextChanged(args, listViewEventData: ListViewEventData) {
		const searchBar = <SearchBar>args.object;
		const listView: RadListView = listViewEventData.object;

		this.searchTerm = searchBar.text;

		if (searchBar.text.length > 0) {
			this.isSearching = true;

			this.leadsOnView = await this._leadRepo.getAllBySearch({
				where: [{firstname: Like("%" + searchBar.text + "%")}, {lastname: Like("%" + searchBar.text + "%")}
					, {cellphoneNumber: Like("%" + searchBar.text + "%")}, {make: Like("%" + searchBar.text + "%")},
					{model: Like("%" + searchBar.text + "%")}]
				, skip: 0,
				take: 10});
			const getSearchCountObj = await this._leadRepo.getSearchCount({
				where: [{firstname: Like("%" + this.searchTerm + "%")}, {lastname: Like("%" + this.searchTerm + "%")}
					, {cellphoneNumber: Like("%" + this.searchTerm + "%")}, {make: Like("%" + this.searchTerm + "%")},
					{model: Like("%" + this.searchTerm + "%")}]
			});
			this.numberOfSearchResults = getSearchCountObj[1];
		} else {
			this.isSearching = false;
			this.reloadList();

		}

		this.hideLoadMoreButton(listView);
	}

	public async onLoadMoreItemsRequested(args: LoadOnDemandListViewEventData) {
			const listView: RadListView = args.object;
			this.totalLeads = await this._leadRepo.getTotal();
			if (!this.isSearching) {
				if (this.totalLeads > this.leadsOnView.length) {
					const batchOfLeads = await this._leadRepo.getAllPaged(this.leadsOnView.length, 10 , { dateCreated: "DESC" });
					this.leadsOnView.push(...batchOfLeads);
					setTimeout(() => {
						listView.notifyLoadOnDemandFinished();
					}, 500);
				} else {
					this.loadingMode = "None";
				}
			} else {
				if (this.numberOfSearchResults > this.leadsOnView.length) {
					const batchOfLeads = await this._leadRepo.getAllBySearch({
						where: [{firstname: Like("%" + this.searchTerm + "%")}, {lastname: Like("%" + this.searchTerm + "%")}
							, {cellphoneNumber: Like("%" + this.searchTerm + "%")}, {make: Like("%" + this.searchTerm + "%")},
							{model: Like("%" + this.searchTerm + "%")}]
						, skip: this.leadsOnView.length,
						take: 10});
					this.leadsOnView.push(...batchOfLeads);
					setTimeout(() => {
						listView.notifyLoadOnDemandFinished();
					}, 500);
				} else {
					this.loadingMode = "None";
					await this._notificationService.infoToastMessage("All leads matching the search has been loaded.", "");
				}
			}

			this.hideLoadMoreButton(listView);

	}

	public hideLoadMoreButton(listView: any) {
		if (this.leadsOnView.length >= this.totalLeads) {
			listView.notifyLoadOnDemandFinished();
		}
	}

	public onClear(args) {
		args.text = "";
		args.dismissSoftInput();
		// this.loadingMode = "Auto";
	}

	public async onAddLeadTap() {
		if (appSettings.getBoolean("canAddEditLeads")) {
			if (this._isLoading !== true) {
				this._isLoading = true;
				this.indicator.show(Config.loaderOptions);
				await this._routerExtensions.navigate(["add-lead"], {relativeTo: this.activeRoute}).then(() => {
						setTimeout(async () => {
							this.indicator.hide();
						}, 500);
					});
				this._isLoading = false;
			}
		} else {
			await alert({
				title: "Unauthorized",
				message: "You don't have permission to perform this action",
				okButtonText: "OK"
			});
		}

	}

	public async onCrmItemTap(args: ListViewEventData) {
		if (appSettings.getBoolean("canAddEditLeads")) {
			const tappedCrmItem = args.view.bindingContext;
			this.indicator.show(Config.loaderOptions);
			await this._routerExtensions.navigate(["/crm/crm-detail",
					tappedCrmItem.generatedId
				], {
					animated: true, transition: {
						name: "slide", duration: 200, curve: "ease"
					}
				}).then(() => {
					setTimeout(async () => {
						this.indicator.hide();
					}, 500);
				});
		} else {
			await alert({
				title: "Unauthorized",
				message: "You don't have permission to perform this action",
				okButtonText: "OK"
			});
		}

	}

	public async onBackButtonTap() {
		await this.ngZone.run(async () => {
			this.indicator.show(Config.loaderOptions);
			await this._routerExtensions.navigateByUrl("/home").then(() => {
					setTimeout(async () => {
						this.indicator.hide();
					}, 500);
				});
		});

	}

	public showVehicleDetails(lead): boolean {
		return +lead.make ? false : lead.make;
	}

	public showLocationDetails(lead): boolean {
		return lead.region;
	}

	public async reloadList() {
		if (!this.isSearching) {
			const that = new WeakRef(this);
			that.get().indicator.show(Config.loaderOptions);

			setTimeout(async () => {
				await that.get().leadService.GetLeads();
				that.get().leadsOnView = [];
				that.get().firstBatchOfLeads = [];
				that.get().firstBatchOfLeads = await this._leadRepo.getAllOrderBy({ dateCreated: "DESC" });
				that.get().leadsOnView.push(...that.get().firstBatchOfLeads);
				that.get().totalLeads = await this._leadRepo.getTotal();
				// that.get().count = 10;

				setTimeout(async () => {
					that.get().indicator.hide();
				}, 500);
			}, 1000);
		}

	}

	private async setupLeadsPage() {
		await this.leadService.GetLeads();
		this.totalLeads = await this._leadRepo.getTotal();
		this.leadsOnView = [];
		this.firstBatchOfLeads = await this._leadRepo.getAllPaged(0, 10 , { dateCreated: "DESC" });

		this.leadsOnView.push(...this.firstBatchOfLeads);

	}

}
