export class CrmLeads {
    id: string;
    dmsLeadId: string;
    firstname: string;
    lastname: string;
    emailAddress: string;
    cellphoneNumber: string;
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    region: string;
    make: string;
    model: string;
    year: string;
    price: string;
    colour: string;
    userId: string;
    branchGuid: string;
    dateCreated: string;
    dateUpdated: string;

    constructor(options: any) {
        this.id = options.id;
        this.dmsLeadId = options.dmsLeadId;
        this.firstname = options.firstname;
        this.lastname = options.lastname;
        this.emailAddress = options.emailAddress;
        this.cellphoneNumber = options.cellphoneNumber;
        this.addressLine1 = options.addressLine1;
        this.addressLine2 = options.addressLine2;
        this.addressLine3 = options.addressLine3;
        this.region = options.region;
        this.make = options.make;
        this.model = options.model;
        this.year = options.year;
        this.price = options.price;
        this.colour = options.colour;
        this.userId = options.userId;
        this.branchGuid = options.branchGuid;
        this.dateCreated = options.dateCreated;
        this.dateUpdated = options.dateUpdated;
    }
}
