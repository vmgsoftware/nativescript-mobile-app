import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import * as appSettings from "tns-core-modules/application-settings";
import { getJSON } from "tns-core-modules/http";
import { Config } from "~/app/Config";
import { Lead } from "~/app/DataLayer/Entities/Lead";
import { LeadRepository } from "~/app/DataLayer/repositories/lead-repository";
import { MmcodesRepository } from "~/app/DataLayer/repositories/mmcodes.repository";
import { SharedService } from "~/app/shared/shared.service";

@Injectable({
	providedIn: "root"
})

export class LeadService {

	public static async GetSyncedLead(lead: Lead) {
		const syncedLead = await this.GetLead(lead.generatedId);

		return syncedLead;
	}

	public static async GetLead(id: number) {
		return getJSON(Config.ApiUrl + "/api/leads/" + id).then((result: Lead) => {
			return result as Lead;
		}, (e) => {
			console.log("Error with leads: ", e);
			alert({
				title: "Could not get leads",
				message: e,
				okButtonText: "OK"
			});

			return null;
		});
	}

	public offlineLeads: Array<any>;

	constructor(public http: HttpClient, private _sharedService: SharedService, private _leadRepo: LeadRepository, private _mmcodesRepo: MmcodesRepository) {

	}

	public async GetLeadById(id: number): Promise<Lead> {
		return this._leadRepo.getById(id);
	}

	public async GetLeadByUniqueId(uniqueId: string): Promise<Lead> {
		return this._leadRepo.getFirstBySearch({where: [{id: uniqueId}]});
	}

	public async GetLeads(): Promise<Array<Lead>> {
		let leads;
		// TO DO FIX HERE!!! Works if below is true
		if (appSettings.getString("leadsupdatedsince", "0") === "0") {
			leads = await this.loadLeads();
			if (leads === null) {
				return this._leadRepo.getAll();
			} else {
				await this._leadRepo.removeAll();
				await this._leadRepo.upsert(leads);
				leads = await this._leadRepo.getAll();

				return leads;

			}
		} else {

			const leadsUpdated: Array<Lead> = await this.loadLeadsUpdatedSince(appSettings.getString("leadsupdatedsince")) as Array<Lead>;
			if (leadsUpdated !== null) {
				const filteredDeletedFalse = leadsUpdated.filter((lead) => lead.deleted === false);

				for (const lead of filteredDeletedFalse) {
					if (lead.make === null) {
						lead.make = "";
					}
					if (lead.model === null) {
						lead.model = "";
					}
					if (lead.variant === null) {
						lead.variant = "";
					}
					if (lead.colour === null) {
						lead.colour = "";
					}
					if (lead.price === null) {
						lead.price = 0;
					}
					if (lead.year === null) {
						lead.year = 0;
					}
				}
				await this._leadRepo.bulkUpsertById(filteredDeletedFalse);
				const filteredDeletedTrue = leadsUpdated.filter((lead) => lead.deleted === true);
				await this._leadRepo.bulkRemoveById(filteredDeletedTrue);
				leads = await this._leadRepo.getAllBySearch({where: [{deleted: false}]});

				return leads;
			} else {
				leads = await this._leadRepo.getAllBySearch({where: [{deleted: false}]});
				for (let i = 0; i < leads.length; i++) {
					leads[i] = await this.translateMmcodesToMakeModelVariant(leads[i]);
				}

				return leads;

			}

		}

	}

	public async loadLeads(): Promise<Array<Lead>> {
		 const headers: HttpHeaders = new HttpHeaders({
			 "Content-Type": "application/json"
		 });
		 const encodedURI = encodeURI(Config.ApiUrl + "/api/leads/" + appSettings.getString("userId"));

		 return this.http.get(encodedURI, {headers})
			 .toPromise()
			 .then(async (value: any) => {
				 const unixTime = Math.round((new Date()).getTime() / 1000);
				 appSettings.setString("leadsupdatedsince", unixTime.toString());
				 for (let i = 0; i < value.data.length; i++) {
				 	value.data[i] = await this.translateMmcodesToMakeModelVariant(value.data[i] as Lead);
				 }

				 return value.data as Array<Lead>;
			 })
			 .catch(async (err: any) => {
				 console.log("Error with lead: ", err);
				 alert({
					 title: "Could not get leads from server",
					 message: err.status + " " + err.statusText,
					 okButtonText: "OK"
				 });
				 console.log("error ::: ", err);

				 return null;
			 });
	}

	public async loadLeadsUpdatedSince(unixTime) {
		 console.log("+++ loadLeadsUpdatedSince(unixTime): ", unixTime);
		 const headers: HttpHeaders = new HttpHeaders({
			 "Content-Type": "application/json"
		 });
		 const encodedURI = encodeURI(Config.ApiUrl + "/api/leads/" + appSettings.getString("userId") + "/updatedsince/" + unixTime);

		 return this.http.get(encodedURI, {headers})
			 .toPromise()
			 .then(async (value: any) => {
				 const dateTimeNowUnix = Math.round((new Date()).getTime() / 1000);
				 appSettings.setString("leadsupdatedsince", dateTimeNowUnix.toString());
				 let myArr: Array<Lead>;
				 myArr = [];
				 if (value.data.length > 0) {

					 for (const lead of value.data as Array<Lead>) {
						const updatedLead = await this.translateMmcodesToMakeModelVariant(lead);
						myArr.push(updatedLead);
					 }
					 if (myArr.length > 0) {
						 return myArr as Array<Lead>;
					 } else {
						 return null;
					 }

				 } else {
					 return null;
				 }
			 })
			 .catch((err: any) => {
				 console.log("Error with leads");
				 alert({
					 title: "Could not get leads from server",
					 message: err.status + " " + err.statusText,
					 okButtonText: "OK"
				 });
				 console.log("error ::: ", err);

				 return null;
			 });
	}

	public async AddLead(lead: Lead) {
		lead.userId =  appSettings.getString("userId");
		const response: any = await this.PostLead(lead);
		if (response === 201) {
			console.log("Lead added online");
		} else {
			await this._leadRepo.upsert(lead);
		}
	}

	public async PostLead(lead: Lead) {
		const headers: HttpHeaders = new HttpHeaders({
			"Content-Type": "application/json"
		});
		const encodedURI = encodeURI(Config.ApiUrl + "/api/leads/" + appSettings.getString("userId"));

		return this.http.post(encodedURI, lead, {headers, observe: "response"})
			.toPromise()
			.then((response: any) => {
				return response.status;
			}, (error) => {
				console.log("error::: ", error);

				return error;
			});

	}

	public async UpdateLead(lead: Lead) {
		const status = await this.PutLead(lead, lead.id);
		if (Number(status) === 204) {
			return 204;
		} else {
			return status;
		}
	}

	public async PutLead(lead: Lead, leadId: string) {
		const headers: HttpHeaders = new HttpHeaders({
			 "Content-Type": "application/json"
		 });
		const encodedURI = encodeURI(Config.ApiUrl + "/api/leads/" + appSettings.getString("userId") + "/" + leadId);

		return this.http.put(encodedURI, lead, {headers, observe: "response"})
			 .toPromise()
			.then((response: any) => {
			 return response.status;
		}, (error) => {
			return error;
		});
	}

	public async RemoveLead(lead: Lead) {
		const status = await this.DeleteLead(lead.id);
		if (Number(status) === 204) {
			await this._leadRepo.remove(lead);
			console.log(await this._leadRepo.getAll());

			return 204;
		} else {
			return status;
		}
	}

	public async DeleteLead(leadId: string) {
		 const headers: HttpHeaders = new HttpHeaders({
			 "Content-Type": "application/json"
		 });
		 const encodedURI = encodeURI(Config.ApiUrl + "/api/leads/" + appSettings.getString("userId") + "/" + leadId);

		 return this.http.delete(encodedURI, {headers, observe: "response"})
			 .toPromise()
			 .then((response: any) => {
				 return response.status;
			 }, (error) => {
				 return error;
			 });
	}

	public async SetupLeads() {
		await this.GetOfflineLeads().then((response: any) => {
			this.offlineLeads = response;
		}, (error) => {
			console.log("Error setting up offline leads.... ", error);
			alert("error setting up offline leads...");
		});
		if (this.offlineLeads.length > 0) {
			await this.PostOfflineLeads();
		}
	}

	public async GetOfflineLeads() {
		return this._leadRepo.getAllBySearch({where: [{isOnline: false}]});
	}

	public async PostOfflineLeads() {
		for (const index in this.offlineLeads) {
			const lead = {
				firstname: this.offlineLeads[index].firstname,
				lastname: this.offlineLeads[index].lastname,
				emailAddress: this.offlineLeads[index].emailAddress,
				cellphoneNumber: this.offlineLeads[index].cellphoneNumber,
				addressLine1: this.offlineLeads[index].addressLine1,
				addressLine2: this.offlineLeads[index].addressLine2,
				addressLine3: this.offlineLeads[index].addressLine3,
				region: this.offlineLeads[index].region,
				make: this.offlineLeads[index].make,
				model: this.offlineLeads[index].model,
				year: this.offlineLeads[index].year,
				price: this.offlineLeads[index].price,
				colour: this.offlineLeads[index].colour,
				userId: this.offlineLeads[index].userId,
				branchGuid: this.offlineLeads[index].branchGuid
			};
			const response = await this.PostLead(lead as Lead);
			if (Number(response) === 201) {
				let myLead = new Lead();
				myLead = this.offlineLeads[index];
				myLead.isOnline = true;
				await this._leadRepo.upsert(myLead);
				alert("posted offline lead.");
			} else {
				let myLead = new Lead();
				myLead = this.offlineLeads[index];
				await this._leadRepo.upsert(myLead);
			}
		}
	}

	public async translateMakeModelvariantToMmcode(lead: Lead) {
		if (lead.make !== "" && lead.make !== null) {
			const makeCode = await this._mmcodesRepo.getFirstBySearch({where: [{make: lead.make}]});
			if (makeCode !== undefined) {
				if (makeCode.mmcode !== undefined) {
					lead.make = makeCode.mmcode;
				}
			}
		}
		if (lead.model !== "" && lead.model !== null) {
			const modelCode = await this._mmcodesRepo.getFirstBySearch({where: [{masterModel: lead.model}]});
			if (modelCode !== undefined) {
				if (modelCode.mmcode !== undefined) {
					lead.model = modelCode.mmcode;
				}
			}
		}
		if (lead.variant !== "" && lead.variant !== null) {
			const variantCode = await this._mmcodesRepo.getFirstBySearch({where: [{variant: lead.variant}]});
			if (variantCode !== undefined) {
				if (variantCode.mmcode !== undefined) {
					lead.variant = variantCode.mmcode;
				}
			}
		}

		return lead;
	}

	public async translateMmcodesToMakeModelVariant(lead: Lead) {
		const myMake = await this._mmcodesRepo.getFirstBySearch({where: [{mmcode: lead.make}]});
		if (myMake !== undefined) {
			if (myMake.make !== undefined) {
				lead.make = myMake.make;
			}

		}
		const myModel = await this._mmcodesRepo.getFirstBySearch({where: [{mmcode: lead.model}]});
		if (myModel !== undefined) {
			if (myModel.masterModel !== undefined) {
				lead.model = myModel.masterModel;
			}

		}
		const myVariant = await this._mmcodesRepo.getFirstBySearch({where: [{mmcode: lead.variant}]});
		if (myVariant !== undefined) {
			if (myVariant.variant !== undefined) {
				lead.variant = myVariant.variant;
			}

		}

		return lead;
	}

}
