export interface ICrmLeads {
    id
    dmsLeadId
    firstname
    lastname
    emailAddress
    cellphoneNumber
    addressLine1
    addressLine2
    addressLine3
    region
    make
    model
    year
    price
    colour
    userId
    branchGuid
    dateCreated
    dateUpdated
}