import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { FloatLabelModule } from "~/app/shared/components/float-label/float-label.module";
import { SpacePipeModule } from "~/app/shared/spacePipe.module";
import { AddLeadComponent } from "./add-lead/add-lead.component";
import { CrmDetailComponent } from "./crm-detail/crm-detail.component";
import { CrmRoutingModule } from "./crm-routing.module";
import { CrmComponent } from "./crm.component";

@NgModule({
	imports: [
		NativeScriptCommonModule,
		NativeScriptFormsModule,
		NativeScriptUIListViewModule,
		CrmRoutingModule,
		NativeScriptFormsModule,
		NativeScriptHttpClientModule,
		SpacePipeModule,
		FloatLabelModule
	],
	declarations: [
		CrmComponent,
		CrmDetailComponent,
		AddLeadComponent
	],
	schemas: [
		NO_ERRORS_SCHEMA
	],
	providers: []
})
export class CrmModule {
}
