import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AddLeadComponent } from "./add-lead/add-lead.component";
import { CrmDetailComponent } from "./crm-detail/crm-detail.component";
import { CrmComponent } from "./crm.component";

const routes: Routes = [
    { path: "", component: CrmComponent },
    { path: "crm-detail/:id", component: CrmDetailComponent },
    { path: "crm-detail/push/:uniqueId", component: CrmDetailComponent },
    { path: "add-lead", component: AddLeadComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class CrmRoutingModule {
}
