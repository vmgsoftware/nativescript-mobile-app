import { Component, ElementRef, NgZone, OnInit, Renderer2, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import * as application from "application";
import { PageRoute, RouterExtensions } from "nativescript-angular/router";
import * as appSettings from "tns-core-modules/application-settings";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import { action, alert, confirm } from "tns-core-modules/ui/dialogs";
import { Color } from "tns-core-modules/ui/frame";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
import { TextField } from "ui/text-field";
import { LeadService } from "~/app/crm/shared/lead.service";
import { Lead } from "~/app/DataLayer/Entities/Lead";
import { MmcodesRepository } from "~/app/DataLayer/repositories/mmcodes.repository";
import { CarService } from "../../DataLayer/car.service";

import { LoadingIndicator } from "@nstudio/nativescript-loading-indicator";
import { Page } from "ui/page";
import { Config } from "~/app/Config";
import { StringHelper } from "~/app/shared/helpers/string-helper";
import { NotificationService } from "~/app/shared/notification.service";
import { SharedService } from "~/app/shared/shared.service";

const regionsList = ["Eastern Cape", "Free State", "Gauteng", "KwaZulu-Natal", "Limpopo",
	"Mpumalanga", "Northern Cape", "North West", "Western Cape"];

@Component({
	selector: "CrmDetail",
	moduleId: module.id,
	templateUrl: "./crm-detail.component.html",
	styleUrls: ["./crm-detail.component.scss"]
})
export class CrmDetailComponent implements OnInit {
	@ViewChild("vehicleDetailsFilter", {static: false}) public vehicleDetailsFilter: ElementRef;
	@ViewChild("modelDetailsFilter", {static: false}) public modelDetailsFilter: ElementRef;

	public listItems: any = [];

	get lead(): Lead {
		switch (this._lead.region) {
			case "Eastern Cape": {
				this.selectedIndex = 0;
				break;
			}
			case "Free State": {
				this.selectedIndex = 1;
				break;
			}
			case "Gauteng": {
				this.selectedIndex = 2;
				break;
			}
			case "KwaZulu-Natal": {
				this.selectedIndex = 3;
				break;
			}
			case "Limpopo": {
				this.selectedIndex = 4;
				break;
			}
			case "Mpumalanga": {
				this.selectedIndex = 5;
				break;
			}
			case "Northern Cape": {
				this.selectedIndex = 6;
				break;
			}
			case "North West": {
				this.selectedIndex = 7;
				break;
			}
			case "Western Cape": {
				this.selectedIndex = 8;
				break;
			}
			default: {
				this.selectedIndex = 0;
				break;
			}
		}

		return this._lead;
	}

	public _lead: Lead = new Lead();
	public indicator = new LoadingIndicator();
	public direction: number;
	public tabSelectedIndex: number = 0;
	public selectedIndex: number;
	public regions: Array<string>;
	public years: Array<string>;
	public platformIOS = isIOS;
	public platformAndroid = isAndroid;

	public categories: Array<string>;
	public makes: Array<string>;
	public models: Array<string>;
	public variants: Array<string>;

	public category: string = "";
	public _isLoading: boolean = true;
	private validationMessage: string = "";
	private selectedFilter: string = "";

	constructor(private _pageRoute: PageRoute,
				         private _routerExtensions: RouterExtensions, private  renderer: Renderer2, private  _carService: CarService, private _leadService: LeadService,
				         private _mmCodesRepo: MmcodesRepository, private activatedRoute: ActivatedRoute, private router: Router, private page: Page, private ngZone: NgZone,
				         private _sharedService: SharedService, private _notificationService: NotificationService) {
		if (isAndroid) {
			this.page.on(application.AndroidApplication.activityBackPressedEvent, this.onBackButtonTap, this);
		}
	}

	public changeRegion() {
		this.selectedFilter = "Region";
		this.listItems = this.regions;
		this.vehicleDetailsFilter.nativeElement.show();
	}

	public changeYear() {
		this.selectedFilter = "Year";
		this.listItems = this.years;
		this.vehicleDetailsFilter.nativeElement.show();
	}

	public onSwipe(args: SwipeGestureEventData) {
		this.direction = args.direction;
		if (this.tabSelectedIndex !== 2 && this.direction === 2) {
			this.tabSelectedIndex = this.tabSelectedIndex + 1;
		} else if (this.tabSelectedIndex !== 0 && this.direction === 1) {
			this.tabSelectedIndex = this.tabSelectedIndex - 1;
		}
	}

	public async ngOnInit() {
		let fromPush: boolean = false;
		let leadId: number;
		let leadUniqueId: string;

		leadId = Number(this.activatedRoute.snapshot.paramMap.get("id"));
		leadUniqueId = this.activatedRoute.snapshot.paramMap.get("uniqueId");

		if (leadId) {
			fromPush = false;
		} else if (leadUniqueId) {
			fromPush = true;
		}

		if (fromPush) {
			await this._leadService.GetLeads();
			const lead: Lead = await this._leadService.GetLeadByUniqueId(leadUniqueId);
			leadId = Number(lead.generatedId);

		}

		this._lead = await this._leadService.GetLeadById(leadId);

		console.log(this._lead);

		if (this._lead.year === 0) {
			this._lead.year = null;
		}

		if (this._lead.price === 0) {
			this._lead.price = null;
		}

		const curYear = new Date();
		const curYearNum = curYear.getFullYear();
		this.regions = [];
		this.tabSelectedIndex = 0;
		for (const region of regionsList) {
			this.regions.push(region);
		}
		this.years = [];
		for (let i = 0; i < 100; i++) {
			this.years.push((curYearNum - i).toString());
		}

		await this.populateCategories();
		await this.getVehicleCategory();
		await this.populateModel();

		if (this.lead.model !== null && this.lead.model !== "") {
			await this.populateVariant();
		}

		await this.populateMakes();
		this._isLoading = false;
	}

	public async updateLead() {
		if (this.formIsValid()) {
			let dialogRes: boolean = false;
			const options = {
				title: "Update lead",
				message: "Are you sure you want to update lead?",
				okButtonText: "Yes",
				cancelButtonText: "No"
			};

			await confirm(options).then((result: boolean) => {
				if (result) {
					dialogRes = true;
				}
			});
			if (dialogRes) {

				let leadToUpdate = {
					id: this.lead.id,
					dmsLeadId: this.lead.dmsLeadId,
					firstname: this.lead.firstname,
					lastname: this.lead.lastname,
					emailAddress: this.lead.emailAddress,
					cellphoneNumber: this.lead.cellphoneNumber,
					addressLine1: this.lead.addressLine1,
					addressLine2: this.lead.addressLine2,
					addressLine3: this.lead.addressLine3,
					region: this.lead.region,
					make: this.lead.make,
					model: this.lead.model,
					variant: this.lead.variant,
					year: this.lead.year !== null ? this.lead.year : 0,
					price: this.lead.price !== null ? this.lead.price : 0,
					colour: this.lead.colour,
					userId: this.lead.userId,
					branchGuid: this.lead.branchGuid
				};
				leadToUpdate = await this._leadService.translateMakeModelvariantToMmcode(leadToUpdate as Lead);
				await this._leadService.UpdateLead(leadToUpdate as Lead).then(async (res) => {
					if (res !== 200) {
						await this._notificationService.errorToastMessage("Could not update lead, please contact support", "");

						return;
					} else {
						setTimeout(async () => {
							await this._routerExtensions.navigate(["/crm"], {
								transition: {
									name: "fade"
								}
							});
						}, 1000);
						await this._notificationService.successToastMessage("Update Successful", `${leadToUpdate.firstname} ${leadToUpdate.lastname} updated successfully`);
					}
				});
			}
		} else {
			await this._notificationService.warningToastMessage("Required Fields", this.validationMessage, 5000);
		}
	}

	public async deleteLead() {
		if (appSettings.getBoolean("canDeleteLeads")) {
			let dialogRes: boolean = false;
			const options = {
				title: "Delete lead",
				message: "Are you sure you want to delete lead?",
				okButtonText: "Yes",
				cancelButtonText: "No"
			};
			await confirm(options).then((result: boolean) => {
				dialogRes = result;
			});
			if (dialogRes) {
				const leadToDelete = {
					id: this.lead.id,
					generatedId: this.lead.generatedId,
					dmsLeadId: this.lead.dmsLeadId,
					firstname: this.lead.firstname,
					lastname: this.lead.lastname,
					emailAddress: this.lead.emailAddress,
					cellphoneNumber: this.lead.cellphoneNumber,
					addressLine1: this.lead.addressLine1,
					addressLine2: this.lead.addressLine2,
					addressLine3: this.lead.addressLine3,
					region: this.lead.region,
					make: this.lead.make,
					model: this.lead.model,
					variant: this.lead.variant,
					year: this.lead.year,
					price: this.lead.price,
					colour: this.lead.colour,
					userId: this.lead.userId,
					branchGuid: this.lead.branchGuid
				};
				const res = await this._leadService.RemoveLead(leadToDelete as Lead);
				if (Number(res) !== 204) {
					await alert("Could not delete lead.");
				}
				this.indicator.show(Config.loaderOptions);
				await this._routerExtensions.navigate(["/crm"], {
					transition: {
						name: "fade"
					}
				}).then(() => {
					setTimeout(async () => {
						this.indicator.hide();
					}, 500);
				});
			}
		} else {
			await alert({
				title: "Unauthorized",
				message: "You don't have permission to perform this action",
				okButtonText: "OK"
			});
		}

	}

	public async onBackButtonTap() {
		await this.ngZone.run(async () => {
			this.indicator.show(Config.loaderOptions);
			await this._routerExtensions.navigate(["/crm"], {
				transition: {
					name: "fade"
				}
			}).then(() => {
				setTimeout(async () => {
					this.indicator.hide();
				}, 500);
			});
		});

	}

	public onFirstNameReturnPress() {
		const element = this.renderer.selectRootElement("#lastName");
		setTimeout(() => element.focus(), 0);

	}

	public onLastNameReturnPress() {
		const element = this.renderer.selectRootElement("#cellphoneNumber");
		setTimeout(() => element.focus(), 0);

	}

	public TextFieldFocus(args) {
		const textField = <TextField>args.object;
		textField.borderBottomColor = new Color("green");
	}

	public TextFieldFocusLost(args) {
		const textField = <TextField>args.object;
		textField.borderBottomColor = new Color("gray");
	}

	public onAddline1ReturnPress(args) {
		const element = this.renderer.selectRootElement("#addressLine2");
		setTimeout(() => element.focus(), 0);

	}

	public onAddline2ReturnPress(args) {
		const element = this.renderer.selectRootElement("#addressLine3");
		setTimeout(() => element.focus(), 0);

	}

	public onAddline3ReturnPress(args) {
		const element = this.renderer.selectRootElement("#region");
		setTimeout(() => element.focus(), 0);

	}

	public formIsValid(): boolean {
		this.validationMessage = "Please complete the following fields: \n\n";
		let valid: boolean = false;
		const regexpEmail = new RegExp("[\\w-]+@([\\w-]+\\.)+[\\w-]+");
		const validEmail = regexpEmail.test(this.lead.emailAddress);
		const regexpPrice = new RegExp("^[0-9]*$");
		let priceToCheck = this.lead.price;
		if (priceToCheck === null) {
			priceToCheck = 0;
		}
		const validPrice = regexpPrice.test(priceToCheck.toString());

		if (this.lead.firstname === "") {
			this.validationMessage += "First Name is required. \n";
		}

		if (this.lead.cellphoneNumber === "") {
			this.validationMessage += "Cellphone Number is required. \n";
		}

		if (this.lead.cellphoneNumber.length < 10 && this.lead.cellphoneNumber !== "") {
			this.validationMessage += "Cellphone Number needs to have a minimum of 10 characters. \n";
		}

		if (this.lead.emailAddress === "") {
			this.validationMessage += "Email Address is required. \n";

		}

		if (!validEmail && this.lead.emailAddress !== "") {
			this.validationMessage += "Email Address is incorrect. \n";
		}

		if (this.lead.firstname !== "" && this.lead.cellphoneNumber.length >= 10 &&
			validEmail && validPrice) {
			valid = true;

			return valid;
		}

		return valid;

	}

	public async changeCategory() {
		this.selectedFilter = "Category";
		this.listItems = this.categories;
		this.vehicleDetailsFilter.nativeElement.show();
	}

	public async changeMake() {
		if (this.category !== "") {
			if (this.makes.length > 0) {
				this.selectedFilter = "Make";
				this.listItems = this.makes;
				this.vehicleDetailsFilter.nativeElement.show();
			} else {
				await this._notificationService.infoToastMessage(`Vehicle category ${this.category} has no makes`, "");
			}
		} else {
			await this._notificationService.warningToastMessage("Selection Required", "Please select a Category.");
		}
	}

	public async changeModel() {
		if (this.lead.make !== "") {
			if (this.models.length > 0) {
				this.selectedFilter = "Model Description";
				this.listItems = this.models;
				this.modelDetailsFilter.nativeElement.show();
			} else {
				await this._notificationService.infoToastMessage(`Vehicle make ${this.lead.make} has no models`, "");
			}
		} else {
			await this._notificationService.warningToastMessage("Selection Required", "Please select a Make.");
		}

	}

	public async changeVariant() {
		if (this.lead.model !== "") {
			if (this.variants.length > 0) {
				this.selectedFilter = "Model";
				this.listItems = this.variants;
				this.vehicleDetailsFilter.nativeElement.show();
			} else {
				await this._notificationService.infoToastMessage(`Vehicle Model ${this.lead.model} has no variants`, "");
			}

		} else {
			await this._notificationService.warningToastMessage("Selection Required", "Please select a Model Description.");
		}

	}

	public async vehicleDetailsFilterSelected(args) {
		if (this.selectedFilter === "Region") {
			this.regionSelected(args);
		}

		if (this.selectedFilter === "Year") {
			this.yearSelected(args);
		}

		if (this.selectedFilter === "Category") {
			this.categorySelected(args);
		}

		if (this.selectedFilter === "Make") {
			this.makeSelected(args);
		}

		if (this.selectedFilter === "Model Description") {
			this.modelDescriptionSelected(args);
		}

		if (this.selectedFilter === "Model") {
			this.variantSelected(args);
		}

	}

	public async regionSelected(args) {
		const index = this.regions.indexOf(args.selectedItem);
		this.lead.region = this.regions[index];
	}

	public async yearSelected(args) {
		const index = this.years.indexOf(args.selectedItem);
		this.lead.year = Number(this.years[index]);
	}

	public async categorySelected(args) {
		const index = this.categories.indexOf(args.selectedItem);
		// Reset form to default/empty
		this.categoryReselected();
		this.category = this.categories[index];
		await this.populateMakes();
	}

	public async makeSelected(args) {
		const index = this.makes.indexOf(args.selectedItem);
		// Reset form to default/empty
		this.makeReselected();
		this.lead.make = this.makes[index];
		await this.populateModel();
	}

	public async modelDescriptionSelected(args) {
		const index = this.models.indexOf(args.selectedItem);
		// Reset form to default/empty
		this.modelReselected();
		this.lead.model = this.models[index];
		await this.populateVariant();
	}

	public async variantSelected(args) {
		const index = this.variants.indexOf(args.selectedItem);
		this.lead.variant = this.variants[index];
	}

	public clearVehicleDetails() {
		this.category = "";
		this.lead.make = "";
		this.lead.model = "";
		this.lead.variant = "";
		this.lead.year = null;
		this.lead.price = null;
		this.lead.colour = "";
	}

	public priceChanged(args) {
		const textField = <TextField>args.object;
		if (textField.text === "") {
			this.lead.price = 0;
		}
	}

	private async getVehicleCategory() {
		this.category = "";
		console.log(this.lead.make);
		if (!StringHelper.isNullOrEmpty(this.lead.make)) {
			 await this._mmCodesRepo.GetCategoryByMakeModelVariant(this.lead.make, this.lead.model, this.lead.variant).then((category) => {
				 this.category = category;
			}).catch((err) => {
				this.category = "";
				this._notificationService.errorToastMessage("Custom vehicle detected", "Vehicle assigned to lead is a custom vehicle whose mmcode does not match any records.");
				console.log(err);
			});
		}
	}

	private categoryReselected() {
		this.lead.make = "";
		this.lead.model = "";
		this.lead.variant = "";
	}

	private makeReselected() {
		this.lead.model = "";
		this.lead.variant = "";
	}

	private modelReselected() {
		this.lead.variant = "";
	}

	private async populateCategories() {
		this.categories = this._sharedService.vehicleCategories;
	}

	private async populateMakes() {
		this.makes = [];
		this.makes = await this._mmCodesRepo.getMakesByCategory(this.category);
	}

	private async populateModel() {
		this.models = [];
		this.models = await this._mmCodesRepo.getMasterModelsByMakeAndCategory(this.lead.make, this.category);
	}

	private async populateVariant() {
		this.variants = [];
		this.variants = await this._mmCodesRepo.getVariantsByMasterModel(this.lead.make, this.lead.model);
	}
}
