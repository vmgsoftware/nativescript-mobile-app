import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import { CardView } from "@nstudio/nativescript-cardview";
import { JwtHelper } from "angular2-jwt";
import * as application from "application";
import {registerElement} from "nativescript-angular";
import { RouterExtensions } from "nativescript-angular/router";
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from "nativescript-ui-sidedrawer";
import { isAndroid, isIOS } from "platform";
import { filter } from "rxjs/operators";
import * as app from "tns-core-modules/application";
import * as appSettings from "tns-core-modules/application-settings";
import { alert, confirm } from "tns-core-modules/ui/dialogs";
import { Frame } from "tns-core-modules/ui/frame";
import * as utils from "utils/utils";
import { Config } from "~/app/Config";
import { LeadService } from "~/app/crm/shared/lead.service";
import {DatabaseHelperService} from "~/app/DataLayer/database-helper";
import { GenericRepository } from "~/app/DataLayer/generic-repository";
import { MmcodesRepository } from "~/app/DataLayer/repositories/mmcodes.repository";
import { ScannerService } from "~/app/scanner/scanner.service";
import {NotificationService} from "~/app/shared/notification.service";
import { SharedService } from "~/app/shared/shared.service";
registerElement('CardView', () => CardView);
registerElement("FilterableListpicker", () => require("nativescript-filterable-listpicker").FilterableListpicker);

@Component({
	moduleId: module.id,
	selector: "ns-app",
	templateUrl: "app.component.html"
})
export class AppComponent implements OnInit {

	get sideDrawerTransition(): DrawerTransitionBase {
		return this._sideDrawerTransition;
	}

	get picture(): any {
		return appSettings.getString("picture", "");
	}

	get nickname(): any {
		return appSettings.getString("nickname", "");
	}

	get name(): any {
		return appSettings.getString("name", "");
	}

	get accessToken(): any {
		return appSettings.getNumber("accessTokenDate", 12);
	}

	get needToLogIn(): boolean {
		if (appSettings.getString("accessToken", "0") === "0") {
			return true;
		} else {
			return false;
		}
	}

	jwtHelper: JwtHelper = new JwtHelper();
	private _activatedUrl: string;
	private _sideDrawerTransition: DrawerTransitionBase;

	constructor(private router: Router, private routerExtensions: RouterExtensions, private _sharedService: SharedService,
				private  _leadService: LeadService, private _scannerService: ScannerService, private _mmCodesRepo: MmcodesRepository,
				public http: HttpClient, private _databaseHelperService: DatabaseHelperService, private _notificationService: NotificationService) {
		router.events.subscribe(() => {
			if (isIOS) {
				Frame.topmost().nativeView.endEditing(true);
			}
			if (isAndroid) {
				utils.ad.dismissSoftInput();
			}
		});
	}

	async ngOnInit() {
		if (isAndroid) {
			application.android.on(application.AndroidApplication.activityBackPressedEvent,
				(args: application.AndroidActivityBackPressedEventData) => {
					const page = Frame.topmost().currentPage;
					if (page.hasListeners(application.AndroidApplication.activityBackPressedEvent)) {
						args.cancel = true;
						page.notify({
							eventName: application.AndroidApplication.activityBackPressedEvent,
							object: page
						});
					}
				});
		}

		await this._sharedService.getApiEndpoints();
		await this._databaseHelperService.createDbConnection();
		this._sharedService.homeLoading = true;
		await this.setupCodes();
		this._sharedService.homeLoading = false;
		await this._scannerService.SetupScanner();
		await this._leadService.SetupLeads();
		if (appSettings.getString("accessToken", "0") !== "0") {
			await this._sharedService.registerForPushNotifications();
		}

		this._sharedService.nickname = appSettings.getString("nickname");
		this._sharedService.name = appSettings.getString("name");
		this._sharedService.sub = appSettings.getString("sub");
		this._sharedService.picture = appSettings.getString("picture");
		this._sharedService.updated_at = appSettings.getString("updated_at");
		this._sideDrawerTransition = new SlideInOnTopTransition();
		this.router.events
			.pipe(filter((event: any) => event instanceof NavigationEnd))
			.subscribe((event: NavigationEnd) => this._activatedUrl = event.urlAfterRedirects);
	}

	async setupCodes() {
		const currMonth = new Date().getMonth();
		if (appSettings.getString("accessToken", "0") !== "0") {
			if ((appSettings.getString("mmcodesdatedsince", "0") === "0") || currMonth !== appSettings.getNumber("monthUpdatedLast", 12)) {
				await this._mmCodesRepo.setupMMCodes(appSettings.getString("mmcodesdatedsince", "0"));
			}
			await this._sharedService.getAllVehicleCategories();
		}
		await this._mmCodesRepo.populateMakes();


	}

	isComponentSelected(url: string): boolean {
		return this._activatedUrl === url;
	}

	async onNavItemTap(navItemRoute: string): Promise<void> {
		if (appSettings.getString("accessToken", "0") !== "0" || navItemRoute === "/support" || navItemRoute === "/home") {
			if ((navItemRoute === "/showroom" && appSettings.getBoolean("canViewStock", false)) ||
				(navItemRoute === "/crm" && appSettings.getBoolean("canViewLeads", false)) ||
				(navItemRoute === "/scanned-disc" && appSettings.getBoolean("canScanDisks", false)) ||
				(navItemRoute === "/transunion-pricing" && appSettings.getBoolean("canUseTransunion", false) && appSettings.getBoolean("transunionSubscriptionStatus", false)) ||
				(navItemRoute === "/support") || (navItemRoute === "/home")) {
				await this.routerExtensions.navigate([navItemRoute], {
					transition: {
						name: "fade"
					}
				});
				const sideDrawer = <RadSideDrawer>app.getRootView();
				sideDrawer.closeDrawer();
			} else {
				await this._notificationService.infoToastMessage("Permissions Required", "You don't have permission to perform this action");
			}
		} else {
			await this._notificationService.infoToastMessage("Permissions Required", "You must be logged in to access this feature.");
		}

	}

	async signOut() {
		const options = {
			title: "Sign out?",
			message: "Are you sure you want to Sign Out?",
			okButtonText: "Yes",
			cancelButtonText: "No"
		};

		confirm(options).then(async (result: boolean) => {
			if (result) {
				const headers: HttpHeaders = new HttpHeaders({
					"Content-Type": "application/json"
				});
				await this.http.put(Config.ApiUrl + "/api/users/unregisterdeviceid", {
					headers,
					observe: "response"
				}).toPromise();

				this._sharedService.totalLeads = 0;
				this._sharedService.totalVehicles = 0;

				await this.routerExtensions.navigate(["/home"], {
					transition: {
						name: "fade"
					}
				});

				const sideDrawer = <RadSideDrawer>app.getRootView();
				sideDrawer.closeDrawer();
				const mmcodes = appSettings.getString("mmcodesdatedsince", "0");
				appSettings.clear();
				appSettings.setString("mmcodesdatedsince", mmcodes);
				await this._sharedService.clearAllTablesExceptMmcodes();

			}
		});
	}

	get version(): string {
		return Config.appVersion;
	}

}
