import { NgModule } from "@angular/core";
import { SpacePipe } from "~/app/shared/currencySpace.pipe";

@NgModule({
	declarations: [
		SpacePipe
	],

	exports: [
		SpacePipe
	]
})
export class SpacePipeModule { }
