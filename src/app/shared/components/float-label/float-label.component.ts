import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color";

@Component({
	selector: "FloatLabel",
	moduleId: module.id,
	template: `
        <GridLayout rows="60" class="p-10">

            <Label #label [text]="placeholder"
                   verticalAlignment="center"
                   style="font-weight: bold; opacity: 0.4"></Label>
            <TextField
                    #textField
                    [text]="text"
                    (focus)="onFocus()" (blur)="onBlur()"
                    [secure]="secure"
					[editable]="isEditable"
                    (textChange)="onTextChange()"
					(tap)="onTap()"></TextField>

            <label *ngIf="showPasswordIcon && showPasswordSecured"
                   text="&#xf070;"
                   style="font-family: FontAwesome; font-size: 20px;"
                   class="filterTextBoxLabel"
                   verticalAlignment="center"
                   horizontalAlignment="right"
                   (tap)="toggleShow()"></label>

            <label *ngIf="showPasswordIcon && !showPasswordSecured"
                   text="&#xf06e;"
                   class="filterTextBoxLabel"
                   style="font-family: FontAwesome; font-size: 20px;"
                   verticalAlignment="center"
                   horizontalAlignment="right"
				   (tap)="toggleShow()"></label>
        </GridLayout>
    `
})
export class FloatLabelComponent {
	@Input() public placeholder: string;
	@Input() public secure: boolean = false;
	@Input() public showPasswordIcon: boolean = false;
	@Input() public isEditable: boolean = true;
	@Input() public keyboardNumberFormat: boolean = false;
	@Input() public text: string;
	@Output() public textChange: EventEmitter<string> = new EventEmitter<string>();
	@Output() public tap: EventEmitter<void> = new EventEmitter<void>();

	public showPasswordSecured: boolean;
	public hasFired: boolean = false;
	@ViewChild("label", {static: false}) private label: ElementRef;
	@ViewChild("textField", {static: false}) private textField: ElementRef;

	public async onFocus() {
		const textField = this.textField.nativeElement;
		await this.animate(true);
		// set the border bottom color to green to indicate focus
		textField.borderBottomColor = new Color("#448744");
	}

	public async  animate(animate: boolean) {
		if (animate) {
			const label = this.label.nativeElement;

			label.color = new Color("#448744");
			label.size = "16px";

			await label.animate({
				translate: { x: 0, y: - 25 },
				opacity: 1
			});

		}
	}

	public onBlur() {
		if (! this.hasFired) {
			this.hasFired = true;
			const label = this.label.nativeElement;
			const textField = this.textField.nativeElement;

			// if there is text in our input then don't move the label back to its initial position.
			if (textField.text === "" || textField.text === undefined || textField.text === null) {
				label.color = new Color("black");
				label.size = "16px";

				label.animate({
					translate: { x: 0, y: 0 },
					opacity: 0.4
				});

			} else {
				label.color = new Color("#448744");
				label.size = "16px";
			}
			// reset border bottom color.
			textField.borderBottomColor = new Color("#cec8c8");
			textField.dismissSoftInput();
		}
		this.hasFired = false;

	}

	public toggleShow() {
		const isSecure: boolean = !this.textField.nativeElement.secure;

		this.showPasswordSecured = isSecure;
		this.textField.nativeElement.secure = isSecure;
	}

	public onTextChange() {
		setTimeout(() => {
			const textField = this.textField.nativeElement;
			if (textField.text !== "" && textField.text !== undefined && textField.text !== null) {
				this.animate(true);
			}
			this.textChange.emit(textField.text);
		}, 100);
	}

	public onTap(): void {
		if (this.keyboardNumberFormat) {
			this.textField.nativeElement.keyboardType = "number";
		}
		this.tap.emit();
	}
}
