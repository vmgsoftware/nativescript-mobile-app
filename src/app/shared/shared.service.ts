import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable, NgZone } from "@angular/core";
import { Router } from "@angular/router";
import { ModalDialogParams, RouterExtensions } from "nativescript-angular";
import { Message, messaging } from "nativescript-plugin-firebase/messaging";
import * as appSettings from "tns-core-modules/application-settings";
import { getJSON } from "tns-core-modules/http";
import { isAndroid } from "tns-core-modules/platform";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Config } from "~/app/Config";
import { GenericRepository } from "~/app/DataLayer/generic-repository";
import { Car } from "~/app/DataLayer/Entities/Car";
import { CarImage } from "~/app/DataLayer/Entities/CarImage";
import { Lead } from "~/app/DataLayer/Entities/Lead";
import { MMCode } from "~/app/DataLayer/Entities/MMCode";
import { ScannedDisc } from "~/app/DataLayer/Entities/ScannedDisc";
import {MmcodesRepository} from "~/app/DataLayer/repositories/mmcodes.repository";
import {CarRepository} from "~/app/DataLayer/repositories/car-repository";
import {ScannedDiscRepository} from "~/app/DataLayer/repositories/scanned-disc-repository";
import {CarImageRepository} from "~/app/DataLayer/repositories/car-image-repository";
import {LeadRepository} from "~/app/DataLayer/repositories/lead-repository";
import {DatabaseHelperService} from "~/app/DataLayer/database-helper";
import {LoadingIndicator} from "@nstudio/nativescript-loading-indicator";

const Sqlite = require("nativescript-sqlite");
const firebase = require("nativescript-plugin-firebase");

@Injectable({
	providedIn: "root"
})
export class SharedService {
	static isOnline: boolean = false;
	nickname: string = "";
	name: string = "";
	sub: string = "";
	picture: string = "";
	updated_at: string = "";
	mmcodes: Array<any>;
	makes: Array<any>;
	models: Array<any>;
	loading: boolean = false;
	homeLoading: boolean = false;
	totalLeads: number;
	totalVehicles: number;
	public indicator = new LoadingIndicator();
	vehicleCategories: Array<string> = [];

	private database: any;

	constructor(private _ngZone: NgZone, private _routerExtensions: RouterExtensions, private _http: HttpClient, public http: HttpClient, private router: Router, private routerExtensions: RouterExtensions
		, private ngZone: NgZone, private _mmCodesRepo: MmcodesRepository, private _leadRepo: LeadRepository, private _carRepo: CarRepository
		, private _scannedDiscRepo: ScannedDiscRepository, private _carImageRepo: CarImageRepository, private _databaseHelper: DatabaseHelperService) {
	}

	async setupDb() {
		this.loading = true;
		this.mmcodes = [];
		this.database = await this.connectToDb();
		await this.createMMCodeTblIfNotExists(this.database);

		const mmcodeCount = await this.countRecords(this.database);
		if (mmcodeCount === 0 || mmcodeCount === null) {
			this.mmcodes = await this.getMMcodes();
			await this.insertAllMMCodes();
		}
		await this.fetchMakes();
		this.loading = false;

	}

	connectToDb() {
		return new Sqlite("my.db").then((db) => {
			return db;
		}, (error) => {
			console.log("OPEN DB ERROR", error);
			return null;
		});

	}

	createMMCodeTblIfNotExists(db) {
		return db.execSQL("CREATE TABLE IF NOT EXISTS mmcodes (id INTEGER PRIMARY KEY AUTOINCREMENT, mmcode TEXT, make TEXT, model TEXT, variant TEXT, dateCreated TEXT, dateUpdated)").then((id) => {
			console.log("createMMCodeTblIfNotExists");
		}, (error) => {
			console.log("error creating table mmcodes : ", error);
		});

	}

	async insertAllMMCodes() {
		this.loading = true;
		for (const i in this.mmcodes) {
			await this.insertMMCode(i);
		}
		this.loading = false;

	}

	insertMMCode(index) {
		return this.database.execSQL("INSERT INTO mmcodes (mmcode, make, model, variant, dateCreated, dateUpdated) VALUES (?, ?, ?, ?, ?, ?)", [this.mmcodes[index].mmcode, this.mmcodes[index].make, this.mmcodes[index].model, this.mmcodes[index].variant, this.mmcodes[index].dateCreated, this.mmcodes[index].dateUpdated]).then((id) => {
		}, (error) => {
			console.log("INSERT ERROR", error);
		});

	}

	async countRecords(db) {
		return db.all("SELECT count(*) from mmcodes").then((rows) => {
			return Number(rows);
		}, (error) => {
			console.log("SELECT ERROR", error);
			return null;
		});
	}

	fetchMakes() {
		return this.database.all("SELECT DISTINCT  make FROM mmcodes").then((rows) => {
			this.makes = [];
			for (const row in rows) {
				this.makes.push(rows[row].toString());
			}
		}, (error) => {
			console.log("SELECT ERROR", error);
			this.loading = true;
		});
	}

	fetchModel(selectedMake) {
		this.database.all("SELECT DISTINCT  model FROM mmcodes WHERE make like " + "'" + selectedMake + "'").then((rows) => {
			this.models = [];
			for (const row in rows) {
				this.models.push(rows[row].toString());
			}
		}, (error) => {
			console.log("SELECT ERROR", error);
		});
	}

	getMMcodes() {
		return getJSON(Config.ApiUrl + "/api/mmcode").then((r: any) => {
			appSettings.setString("mmcodes", JSON.stringify(r));
			return r;
		}, (e) => {
			console.log("Error loading /api/mmcode ", e);
			return null;
		});
	}

	async PostTransUnion(disc) {
		const headers: HttpHeaders = new HttpHeaders({
			"Content-Type": "application/json"
		});
		return this.http.post(Config.ApiUrl + "/api/scanneddisks/transunion", disc, {headers, observe: "response"})
			.toPromise()
			.then((response: any) => {
				return response;
			}, (error) => {
				console.log("error::: ", error);
				return error;
			});
	}

	async registerForPushNotifications() {
		try {
			await firebase.init({
				showNotificationsWhenInForeground: true,
				onPushTokenReceivedCallback(token) {
					console.log("Firebase push token: " + token);
				}
			}).then(
				() => {
					console.log("firebase.init done");
				},
				(error) => {
					console.log(`firebase.init error: ${error}`);
				}
			);
		} catch (e) {
			console.log(e);
		}
		messaging.registerForPushNotifications({
			 onPushTokenReceivedCallback: async (token: string) => {
				const headers: HttpHeaders = new HttpHeaders({
					"Content-Type": "application/json"
				});
				await this.http.put(Config.ApiUrl + "/api/users/registerdeviceid/" + token, {
					headers,
					observe: "response"
				}).toPromise()
					.then(async (response: any) => {
						if (response.statusCode === 200) {
							console.log("successful registration for push.");
						} else {
							console.log("could not register device for push notifications");
						}

					}, async (error) => {
						console.log("Push notification error: ", error.error.message);
					});
			}, onMessageReceivedCallback: async (message: Message) => {
			 	if (isAndroid) {
					if (message.foreground) {
						if (message.data.path !== "home") {
							dialogs.confirm({
								title: message.data.message,
								message: "Would you like to view this record now?",
								okButtonText: "Yes",
								cancelButtonText: "No"
							}).then(async (result) => {
								// result argument is boolean
								if (result) {
									await this.ngZone.run(async () => {
										this.indicator.show(Config.loaderOptions);
											await this._routerExtensions.navigate([message.data.path
											], {
												animated: true, transition: {
													name: "slide", duration: 200, curve: "ease"
												}
											}).then(() => {
												setTimeout(async () => {
													this.indicator.hide();
												}, 500);
											});
									});
								}
							});
						} else {
							dialogs.alert({
								title: message.data.message,
								okButtonText: "OK"
							});
						}

					} else {
						await this.ngZone.run(async () => {
							this.indicator.show(Config.loaderOptions);
								await this._routerExtensions.navigate([message.data.path
								], {
									animated: true, transition: {
										name: "slide", duration: 200, curve: "ease"
									}
								}).then(() => {
									setTimeout(async () => {
										this.indicator.hide();
									}, 500);
								});
						});
					}
				} else {
						await this.ngZone.run(async () => {
							if (message.data.path !== "home") {
								this.indicator.show(Config.loaderOptions);
									await this._routerExtensions.navigate([message.data.path
									], {
										animated: true, transition: {
											name: "slide", duration: 200, curve: "ease"
										}
									}).then(() => {
										setTimeout(async () => {
											this.indicator.hide();
										}, 500);
									});
							}

						});
				}

			},
			// Whether you want this plugin to automatically display the notifications or just notify the callback. Currently used on iOS only. Default true.
			 showNotifications: true,
			// Whether you want this plugin to always handle the notifications when the app is in foreground. Currently used on iOS only. Default false.
			 showNotificationsWhenInForeground: true
		}).then(() => {
			console.log("Successfully registered for push notifications.");
		});
	}

	/**
	 * The folloowing section checks clears the Lead and Car table every day, and the entire Db once a month.
	 *  This is done as a premtive measure to ensure the local db and the live db stay in sync
	 */
	async clearAllTablesExceptMmcodes() {
		await this._carRepo.removeAll();
		await this._carImageRepo.removeAll();
		await this._leadRepo.removeAll();
		await this._scannedDiscRepo.removeAll();
	}

	async clearAllTables() {
		await this._databaseHelper.clearDatabase();
	}

	async getApiEndpoints() {
		const encodedURI = encodeURI("https://api.vmgdms.com/vmg/service-urls/v1");
		await this.http.get(encodedURI)
			.toPromise()
			.then(async (response: any) => {
				Config.loginUrl = response.single_sign_on;
				Config.ApiUrl = response.mobile_api_v3;
				Config.transunionUrl = response.transunion;
				appSettings.setString("loginUrl", Config.loginUrl);
				appSettings.setString("ApiUrl", Config.ApiUrl);
				appSettings.setString("transunionUrl", Config.transunionUrl);
			})
			.catch(async (err: any) => {
				console.log("Could not get APi enpoints dynamically : ", err);
				Config.loginUrl = appSettings.getString("loginUrl", Config.loginUrl);
				Config.ApiUrl = appSettings.getString("ApiUrl", Config.ApiUrl);
				Config.ApiUrl = appSettings.getString("transunionUrl", Config.transunionUrl);
			});
	}

	async getTotalLeadsAndShowroom() {
		this.totalLeads = await this._leadRepo.getTotal();
		this.totalVehicles = await this._carRepo.getTotal();
	}

	async getAllVehicleCategories() {
		this.vehicleCategories = await this._mmCodesRepo.getCategories();
	}

}
