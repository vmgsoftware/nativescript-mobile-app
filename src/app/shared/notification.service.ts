/*
	TODO extend functionality: https://github.com/EddyVerbruggen/nativescript-feedback
 */

import { Injectable } from "@angular/core";
import { Feedback } from "nativescript-feedback";
import { Color } from "tns-core-modules/color";

@Injectable({
	providedIn: "root"
})
export class NotificationService {
	private feedback: Feedback;

	public constructor() {
		this.feedback = new Feedback();
	}

	public async infoToastMessage(title: string = "", message: string, titleColor: string = "white") {
		await this.feedback.info({
			title: `${title}`,
			message: `${message}`,
			titleColor: new Color(`${titleColor}`),
			backgroundColor: new Color("#263238")
		});
	}

	public async successToastMessage(title: string = "", message: string, titleColor: string = "white") {
		await this.feedback.success({
			title: `${title}`,
			message: `${message}`,
			titleColor: new Color(`${titleColor}`),
			backgroundColor: new Color("#54d72a")
		});
	}

	public async errorToastMessage(title: string = "", message: string, titleColor: string = "black") {
		await this.feedback.error({
			title: `${title}`,
			message: `${message}`,
			titleColor: new Color(`${titleColor}`),
			backgroundColor: new Color("#f04141")
		});
	}

	public async warningToastMessage(title: string = "", message: string, duration: number = 4000) {
		await this.feedback.warning({
			title: `${title}`,
			message: `${message}`,
			titleColor: new Color("black"),
			messageColor: new Color("black"),
			backgroundColor: new Color("#ffce00"),
			duration
		});
	}
}
