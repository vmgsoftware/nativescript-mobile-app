import { Injectable } from "@angular/core";
import * as email from "nativescript-email";
import { alert } from "ui/dialogs";

@Injectable({
	providedIn: "root"
})

export class EmailService{
	private emailOptions: {};

	constructor(){}

	public async sendEmail(to: string, subject: string = "", body: string = "") {
		this.emailOptions = {
			to: [to],
			subject: subject,
			body: body
		};
		email.available().then(async (available) => {
			if (available) {
				await email.compose(this.emailOptions).then(() => {
				}).catch((error) =>
					console.log(error));
			} else {
				await alert("Could not send email. Please insure that email configuration has been set up on your device.");
			}
		}).catch((error) =>
			console.log(error));
	}
}
