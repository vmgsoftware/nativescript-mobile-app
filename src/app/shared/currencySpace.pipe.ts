import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'space'
})
export class SpacePipe implements PipeTransform {
	transform(value: any, args?: any): any {
		if(value)
			return value.replace(",", " ").substring(0, 1) + ' ' + value.replace(",", " ").substring( 1);
	}
}
