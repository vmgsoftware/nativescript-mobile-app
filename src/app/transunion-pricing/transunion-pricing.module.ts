import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NativeScriptFormsModule } from "nativescript-angular";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { SpacePipeModule } from "~/app/shared/spacePipe.module";

import { FloatLabelModule } from "~/app/shared/components/float-label/float-label.module";
import { TransunionPricingRoutingModule } from "./transunion-pricing-routing.module";
import { TransunionPricingComponent } from "./transunion-pricing.component";

@NgModule({
	imports: [
		NativeScriptCommonModule,
		TransunionPricingRoutingModule,
		FormsModule,
		NativeScriptFormsModule,
		NativeScriptUIListViewModule,
		SpacePipeModule,
		FloatLabelModule
	],
	declarations: [
		TransunionPricingComponent
	],
	schemas: [
		NO_ERRORS_SCHEMA
	]
})
export class TransunionPricingModule { }
