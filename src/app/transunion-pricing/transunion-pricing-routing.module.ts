import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { TransunionPricingComponent } from "./transunion-pricing.component";

const routes: Routes = [
	{ path: "", component: TransunionPricingComponent }
];

@NgModule({
	imports: [NativeScriptRouterModule.forChild(routes)],
	exports: [NativeScriptRouterModule]
})
export class TransunionPricingRoutingModule { }
