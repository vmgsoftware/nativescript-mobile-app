import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { LoadingIndicator } from "@nstudio/nativescript-loading-indicator";
import * as application from "application";
import { RouterExtensions } from "nativescript-angular";
import { isAndroid, isIOS } from "platform";
import { action, alert } from "ui/dialogs";
import { Color, Page } from "ui/page";
import { SearchBar } from "ui/search-bar";
import { TextField } from "ui/text-field";
import { Config } from "~/app/Config";
import { Car } from "~/app/DataLayer/Entities/Car";
import { MmcodesRepository } from "~/app/DataLayer/repositories/mmcodes.repository";
import { NotificationService } from "~/app/shared/notification.service";
import { SharedService } from "~/app/shared/shared.service";

@Component({
	selector: "Transunion-pricing",
	moduleId: module.id,
	templateUrl: "./transunion-pricing.html",
	styleUrls: ["./transunion-pricing.component.scss"]
})

export class TransunionPricingComponent implements OnInit {
	@ViewChild("vehicleDetailsFilter", {static: false}) public vehicleDetailsFilter: ElementRef;
	@ViewChild("modelDetailsFilter", {static: false}) public modelDetailsFilter: ElementRef;

	public listItems: any = [];

	public indicator = new LoadingIndicator();
	public platformIOS = isIOS;
	public platformAndroid = isAndroid;

	public years: Array<string>;
	public makes: Array<string>;
	public models: Array<string>;
	public variants: Array<string>;
	public showRetrievePriceOverlay: boolean = true;
	public mmCode: Array<any>;

	public car: Car;

	public vehicleTradePrice: number;
	public vehicleRetailPrice: number;

	public canEditKM: boolean = false;

	private vehicleCategory: Array<string>;

	public _isLoading: boolean = true;
	private _isLoadingData: boolean = true;
	private _isLoadingPrices: boolean = false;

	private _isAgricultureSelected: boolean = false;
	private selectedFilter: string = "";
	private validationMessage: string = "";

	constructor(private _routerExtensions: RouterExtensions, private page: Page, private _sharedService: SharedService, private _mmcodeRepository: MmcodesRepository,
				         private _notificationService: NotificationService) {
		if (isAndroid) {
			this.page.on(application.AndroidApplication.activityBackPressedEvent, this.onBackButtonTap, this);
		}
	}

	public async ngOnInit() {
		this.populateYears();
		await this.populateVehicleCategories();
		this.car = new Car();

		setTimeout(() => {
			this._isLoading = false;
		}, 500);

	}

	public changeYear() {
		this.selectedFilter = "Year";
		this.listItems = this.years;
		this.vehicleDetailsFilter.nativeElement.show();
	}

	public async vehicleDetailsFilterSelected(args) {
		if (this.selectedFilter === "Year") {
			this.yearSelected(args);
		}

		if (this.selectedFilter === "Category") {
			this.categorySelected(args);
		}

		if (this.selectedFilter === "Make") {
			this.makeSelected(args);
		}

		if (this.selectedFilter === "Model Description") {
			this.modelDescriptionSelected(args);
		}

		if (this.selectedFilter === "Model") {
			this.variantSelected(args);
		}

	}

	public async changeCategory() {
		if (this.car.year) {
			this.selectedFilter = "Category";
			this.listItems = this.vehicleCategory;
			this.vehicleDetailsFilter.nativeElement.show();
		} else {
			await this._notificationService.warningToastMessage("Selection Required", "Please select a Year.");
		}

	}

	public async changeMake() {
		if (this.car.category) {
			if (this.makes.length > 0) {
				this.selectedFilter = "Make";
				this.listItems = this.makes;
				this.vehicleDetailsFilter.nativeElement.show();
			} else {
				await this._notificationService.infoToastMessage(`Vehicle category ${this.car.category} has no makes`, "");
			}
		} else {
			await this._notificationService.warningToastMessage("Selection Required", "Please select a Category.");
		}
	}

	public async changeModel() {
		if (this.car.make) {
			if (this.models.length > 0) {
				this.selectedFilter = "Model Description";
				this.listItems = this.models;
				this.modelDetailsFilter.nativeElement.show();
			} else {
				await this._notificationService.infoToastMessage(`Vehicle make ${this.car.make} has no models`, "");
			}
		} else {
			await this._notificationService.warningToastMessage("Selection Required", "Please select a Make.");
		}

	}

	public async changeVariant() {
		if (this.car.masterModel) {
			if (this.variants.length > 0) {
				this.selectedFilter = "Model";
				this.listItems = this.variants;
				this.vehicleDetailsFilter.nativeElement.show();
			} else {
				await this._notificationService.infoToastMessage(`Vehicle Model ${this.car.model} has no variants`, "");
			}
		} else {
			await this._notificationService.warningToastMessage("Selection Required", "Please select a Model Description.");
		}

	}

	public async yearSelected(args) {
		const index = this.years.indexOf(args.selectedItem);
		// Reset form to default/empty
		this.yearReselected();
		this.car.year = Number(this.years[index]);
	}

	public async categorySelected(args) {
		const index = this.vehicleCategory.indexOf(args.selectedItem);
		// Reset form to default/empty
		this.categoryReselected();
		this.car.category = this.vehicleCategory[index];
		this._isAgricultureSelected = this.car.category === "Agricultural";
		await this.populateMakes();
	}

	public async makeSelected(args) {
		const index = this.makes.indexOf(args.selectedItem);
		// Reset form to default/empty
		this.makeReselected();
		this.car.make = this.makes[index];
		await this.populateModel();
	}

	public async modelDescriptionSelected(args) {
		const index = this.models.indexOf(args.selectedItem);
		// Reset form to default/empty
		this.modelReselected();
		this.car.masterModel = this.models[index];
		await this.populateVariant();
	}

	public async variantSelected(args) {
		const index = this.variants.indexOf(args.selectedItem);
		this.car.variant = this.variants[index];
	}

	public async onBackButtonTap() {
		this.indicator.show(Config.loaderOptions);
		await this._routerExtensions.navigateByUrl("/home").then(() => {
				setTimeout(async () => {
					this.indicator.hide();
				}, 500);
			});
	}

	public TextFieldFocus(args) {
		const textField = <TextField>args.object;
		textField.borderBottomColor = new Color("green");
	}

	public TextFieldFocusLost(args) {
		const textField = <TextField>args.object;
		textField.borderBottomColor = new Color("gray");
	}

	public async getVehiclePrices() {

		if (this.validateRequiredFields()) {
			this.mmCode = await this.getVehicleMMCode();

			if (this.mmCode) {
				await this.getTransunionPrices();
			}
		} else {
			await this._notificationService.warningToastMessage("Required Fields", this.validationMessage,  5000);
		}

	}

	public validateRequiredFields(): boolean {
		if (this._isAgricultureSelected) {
			return this.validateAgricultureFields();
		} else {
			return this.validateTextFields();
		}
	}

	public async getAllCategoriesByYear() {
		await this.populateVehicleCategories();
	}

	public async populateMakes() {
		this.makes = [];
		this.makes = await this._mmcodeRepository.getMakesByCategory(this.car.category);

	}

	public async kmTextChanged(args) {
		const searchBar = <SearchBar>args.object;

		if (searchBar.text !== "") {
			this.showRetrievePriceOverlay = searchBar.text !== this.car.km;
		}
	}

	public clearAllSearchFields() {
		this.car.year = null;
		this.car.category = "";
		this.car.make = "";
		this.car.masterModel = "";
		this.car.variant = "";
		this.car.km = "";
		this.canEditKM = false;

		this.showRetrievePriceOverlay = true;
	}

	public async kmFieldSelected(): Promise<void> {
		if (!this.canEditKM) {
			this.validateRequiredFields();
			await this._notificationService.warningToastMessage("Selection Required", "Please make sure all required fields are selected before editing KM.");
		}
	}

	private validateAgricultureFields(): boolean {
		this.validationMessage = "Please complete the following fields: \n\n";
		let valid: boolean = false;

		if (this.car.year === undefined || this.car.year === 0) {
			this.validationMessage += "Year is required. \n";
		}

		if (this.car.category === undefined || this.car.category === "") {
			this.validationMessage += "Category is required. \n";
		}

		if (this.car.make === undefined || this.car.make === "") {
			this.validationMessage += "Make is required. \n";
		} else {
			this.canEditKM = true;
			valid = true;
		}

		return valid;
	}

	private validateTextFields(): boolean {
		this.validationMessage = "Please complete the following fields: \n\n";
		let valid: boolean = false;

		if (this.car.year === undefined || this.car.year === 0) {
			this.validationMessage += "Year is required. \n";
		}

		if (this.car.category === undefined || this.car.category === "") {
			this.validationMessage += "Category is required. \n";
		}

		if (this.car.make === undefined || this.car.make === "") {
			this.validationMessage += "Make is required. \n";
		}

		if (this.car.masterModel === undefined || this.car.masterModel === "") {
			this.validationMessage += "Model Description is required. \n";
		}

		if (this.car.variant === undefined || this.car.variant === "") {
			this.validationMessage += "Model is required. \n";
		} else {
			this.canEditKM = true;
			valid = true;
		}

		return valid;
	}

	private populateYears(): void {
		const curYear = new Date();
		const curYearNum = curYear.getFullYear();

		this.years = [];
		for (let i = 0; i < 100; i++) {
			this.years.push((curYearNum - i).toString());
		}
	}

	private async populateVehicleCategories() {
		this.vehicleCategory = this._sharedService.vehicleCategories;
	}

	private async populateModel() {
		this.models = [];
		this.models = await this._mmcodeRepository.getMasterModelsByMakeAndYear(this.car.make, this.car.year.toString());
	}

	private async populateVariant() {
		this.variants = [];
		this.variants = await this._mmcodeRepository.getVariantsByMasterModelAndYear(this.car.make, this.car.masterModel, this.car.year.toString());
	}

	private async getTransunionPrices() {
		this._isLoading = true;
		this._isLoadingData = false;
		this._isLoadingPrices = true;

		if (this.car.km === "") {
			this.car.km = undefined;
		}

		const transUnionVehicleData = {
			VIN: this.car.vin,
			Mileage: this.car.km,
			Year: this.car.year,
			MMCode: this.mmCode,
			LicenseNumber: ""
		};
		const response = await this._sharedService.PostTransUnion(transUnionVehicleData) as any;
		if (response.status === 200) {
			this.vehicleTradePrice = Number(response.body.data.tradePrice);
			this.vehicleRetailPrice = Number(response.body.data.retailPrice);

			this.showRetrievePriceOverlay = false;

			this._isLoading = false;
			this._isLoadingData = true;
			this._isLoadingPrices = false;

		} else {
			await alert(({
				title: "TransUnion could not find vehicle.",
				okButtonText: "OK"
			}));
			this._isLoading = false;
			this._isLoadingData = true;
			this._isLoadingPrices = false;
		}
	}

	private async getVehicleMMCode() {
		let whereClause = {};
		if (this._isAgricultureSelected) {
			whereClause = {
				make: this.car.make
			};
		} else {
			whereClause = {
				variant: this.car.variant
			};
		}

		this.mmCode = await this._mmcodeRepository.getAllBySearch({
			where: [whereClause]
		});

		return this.mmCode[0].mmcode;
	}

	private yearReselected() {

		this.car.category = "";
		this.car.make = "";
		this.car.masterModel = "";
		this.car.variant = "";
		this.showRetrievePriceOverlay = true;
		this.canEditKM = false;

	}

	private categoryReselected() {
		this.car.make = "";
		this.car.masterModel = "";
		this.car.variant = "";
		this.showRetrievePriceOverlay = true;
		this.canEditKM = false;
	}

	private makeReselected() {
		this.car.masterModel = "";
		this.car.variant = "";
		this.showRetrievePriceOverlay = true;
		this.canEditKM = false;
	}

	private modelReselected() {
		this.car.variant = "";
		this.showRetrievePriceOverlay = true;
		this.canEditKM = false;
	}

}
