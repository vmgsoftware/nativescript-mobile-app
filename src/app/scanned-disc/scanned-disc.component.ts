import { Component, OnInit } from "@angular/core";
import { LoadingIndicator } from "@nstudio/nativescript-loading-indicator";
import * as application from "application";
import { RouterExtensions } from "nativescript-angular";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { Feedback } from "nativescript-feedback";
import * as appSettings from "tns-core-modules/application-settings";
import { Color } from "tns-core-modules/color";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { action, alert, confirm } from "tns-core-modules/ui/dialogs";
import { TextField } from "tns-core-modules/ui/text-field";
import * as utils from "tns-core-modules/utils/utils";
import { Page } from "ui/page";
import { Config } from "~/app/Config";
import { ScannedDisc } from "~/app/DataLayer/Entities/ScannedDisc";
import { ScannerService } from "~/app/scanner/scanner.service";
import { NotificationService } from "~/app/shared/notification.service";
import { SharedService } from "~/app/shared/shared.service";
import {StringHelper} from "~/app/shared/helpers/string-helper";

@Component({
	selector: "ScannedDisc",
	moduleId: module.id,
	templateUrl: "./scanned-disc.component.html"
})
export class ScannedDiscComponent implements OnInit {
	public indicator = new LoadingIndicator();
	public years: Array<string>;
	public loading: boolean = false;
	public myDisc = new ScannedDisc();
	public disabledTu: boolean = false;
	public platformIOS = isIOS;
	public platformAndroid = isAndroid;
	public scanResult: string = "";
	public decoded: string = "";
	public decoded1: string = "";
	public decoded2: string = "";
	public decoded3: string = "";
	public decoded4: string = "";
	public licenseNo: string = "";
	public regNo: string = "";
	public vehRegNo: string = "";
	public body: string = "";
	public make: string = "";
	public model: string = "";
	public colour: string = "";
	public vin: string = "";
	public engineNo: string = "";
	public expDate: string = "";
	public condition: string = "";
	public scannedDiscTitle: string = "Scanned Disc";
	private feedback: Feedback;

	constructor(public _scannerService: ScannerService, private  _routerExtensions: RouterExtensions, private _sharedService: SharedService,
				         private page: Page, private _notificationService: NotificationService) {
		this.feedback = new Feedback();
		if (isAndroid) {
			this.page.on(application.AndroidApplication.activityBackPressedEvent, this.onBackButtonTap, this);
		}
		this._scannerService.decodedDisc = new ScannedDisc();
	}

	public async ngOnInit(): Promise<void> {
		await this.onScan();
	}

	public async onBackButtonTap() {
		this.indicator.show(Config.loaderOptions);
		await this._routerExtensions.navigateByUrl("/home").then(() => {
				setTimeout(async () => {
					this.indicator.hide();
				}, 500);
			});
	}

	public changeYear() {
		const curYear = new Date();
		const curYearNum = curYear.getFullYear();
		this.years = [];
		for (let i = 0; i < 100; i++) {
			this.years.push((curYearNum - i).toString());
		}
		const options = {
			title: "Year",
			message: "Select year",
			cancelButtonText: "Cancel",
			actions: this.years
		};

		action(options).then((result) => {
			if (result !== "Cancel") {
				this._scannerService.decodedDisc.year = Number(result);
			}
		});
	}

	public changeCondition() {
		const options = {
			title: "Vehicle condition",
			message: "Select vehicle confition",
			cancelButtonText: "Cancel",
			actions: ["Excellent", "Good", "Fair", "Poor"]
		};

		return action(options).then((result) => {
			if (result !== "Cancel") {
				this._scannerService.decodedDisc.condition = result;
			}
		});
	}

	public async postToTransunion() {
		if (appSettings.getBoolean("canUseTransunion", false) && appSettings.getBoolean("transunionSubscriptionStatus", false)) {
			if (this.disabledTu === false) {
				const transUnionDisk = this.checkDiscForNullFields();

				this.loading = true;
				this.indicator.show(Config.loaderOptions);
				const response = await this._sharedService.PostTransUnion(transUnionDisk) as any;
				this.indicator.hide();
				if (response.status === 200) {
					this.loading = false;
					this._scannerService.decodedDisc.mmCode = response.body.data.mmCode;
					this._scannerService.decodedDisc.introDate = response.body.data.introDate;
					this._scannerService.decodedDisc.retailPrice = response.body.data.retailPrice;
					this._scannerService.decodedDisc.tradePrice = response.body.data.tradePrice;
					this._scannerService.decodedDisc.make = response.body.data.make;
					this._scannerService.decodedDisc.model = response.body.data.model;
					this._scannerService.decodedDisc.variant = response.body.data.variant;

				} else {
					this.loading = false;
					await this._notificationService.infoToastMessage("TransUnion could not find vehicle.", "");
				}
			} else {
				await this.feedback.info({
					title: "Transunion check disabled",
					message: "To enable this feature please tap here.",
					titleColor: new Color("white"),
					backgroundColor: new Color("#263238"),
					duration: 5000,
					onTap: async () => {
						this.disabledTu = false;
					}
				});
			}
			this.disabledTu = true;
		} else if (appSettings.getBoolean("transunionSubscriptionStatus", false) === false) {
			dialogs.confirm({
				message: "Aw shucks! It looks as though your VMG TransUnion credentials are still missing. Please visit our blog page to find out how to register for this feature.",
				okButtonText: "Blog Page",
				cancelButtonText: "No Thanks"
			}).then((result) => {
				if (result === true) {
					utils.openUrl("https://www.vmgsoftware.co.za/post/0/Register-for-TU-Pricing-in-VMG");
				} else {
					return;
				}
			});
		} else {
			await alert("Oh no! it looks as though you need your permissions to be set in your VMG DMS to use this feature. Please ask the authorised person (Owner) to email the permission request to support@vmgsoftware.co.za. We’ll get you up and running in no time.");
		}
	}

	public async submitStock() {
		this.loading = true;
		const response = await this._scannerService.PostDisc(this._scannerService.decodedDisc);
		if (response === "success") {
			this.loading = false;

			const options = {
				title: "Stock upload successful",
				message: `${this._scannerService.decodedDisc.make} ${this._scannerService.decodedDisc.model} added to stock. Would you like to scan another vehicle?`,
				okButtonText: "Yes",
				cancelButtonText: "No"
			};

			confirm(options).then(async (result: boolean) => {
				if (result === true) {
					this.ScanAnotherDisc();
				} else {
					await this._routerExtensions.navigateByUrl("/home").then(() => {
						setTimeout(async () => {
							this.indicator.hide();
						}, 500);
					});
				}
			});

		} else {
			this.loading = false;
			await this._notificationService.errorToastMessage(`Stock upload unsuccessful, please contact support`, "");
		}

	}

	public async cancel() {
		return dialogs.confirm({
			title: "Are you sure you want to cancel?",
			message: "Discard scanned information and cancel",
			okButtonText: "Yes",
			cancelButtonText: "No"
		}).then(async (result) => {
			// result argument is boolean
			if (result) {
					await this._routerExtensions.navigateByUrl("/home").then(() => {
						setTimeout(async () => {
							this.indicator.hide();
						}, 500);
					});
			}
		});

	}

	public async onScan() {
		await new BarcodeScanner().scan({
			formats: "PDF_417",
			showFlipCameraButton: false,
			preferFrontCamera: false,
			showTorchButton: true,
			beepOnScan: true,
			torchOn: false,
			resultDisplayDuration: 500,
			// orientation: orientation,
			openSettingsIfPermissionWasPreviouslyDenied: true // ios only
		}).then(async (result) => {
			setTimeout(async () => {
				this.scanResult = result.text;
				const decodedDisc = this.decode(this.scanResult.toString());
				decodedDisc.year = await this._scannerService.getYearByVin(decodedDisc.vin);
				this._scannerService.decodedDisc = decodedDisc;
			}, 500);
			}, async (errorMessage) => {
				 console.log(errorMessage);
			  setTimeout(async () => {
				await this._routerExtensions.navigateByUrl("/home");
			}, 100);
			}
		);
	}

	public decode(encoded: string): ScannedDisc {
		const decoded = encoded.split("%");
		if (decoded.length === 16) {
			this.myDisc.licenseNumber = decoded[6];
			this.myDisc.registrationNumber = decoded[7];
			this.myDisc.description = decoded[8];
			this.myDisc.make = decoded[9];
			this.myDisc.model = decoded[10];
			this.myDisc.colour = decoded[11];
			this.myDisc.vin = decoded[12];
			this.myDisc.engineNumber = decoded[13];
			this.expDate = decoded[14];
		} else if (decoded.length > 16) { // Natus Document has generally more than 16 fields. Only way to split a license disc from a natus document
			this.myDisc.registrationNumber = decoded[6];
			this.myDisc.description = decoded[7];
			this.myDisc.make = decoded[8];
			this.myDisc.model = decoded[9];
			this.myDisc.vin = decoded[10];
			this.myDisc.engineNumber = decoded[11];
			this.expDate = decoded[14];
		}

		this.myDisc.recordLive = false;
		this.myDisc.status = "new";
		this.scannedDiscTitle = this.myDisc.make;
		const curDate = new Date();
		this.myDisc.dateScanned = curDate.toString().substring(0, curDate.toString().length - 15);

		return this.myDisc;

	}

	public TextFieldFocus(args) {
		const textField = <TextField>args.object;
		textField.borderBottomColor = new Color("green");
	}

	public TextFieldFocusLost(args) {
		const textField = <TextField>args.object;
		textField.borderBottomColor = new Color("gray");
	}

	private checkDiscForNullFields(): object {
		const discJson = {};

		if (this._scannerService.decodedDisc.vin !== null && this._scannerService.decodedDisc.vin !== undefined) {
			discJson["VIN"] = this._scannerService.decodedDisc.vin;
		}

		if (this._scannerService.decodedDisc.mileage !== null && this._scannerService.decodedDisc.mileage !== undefined) {
			discJson["Mileage"] = this._scannerService.decodedDisc.mileage.toString() === "" ? 0 : this._scannerService.decodedDisc.mileage;
		}

		if (this._scannerService.decodedDisc.registrationNumber !== null && this._scannerService.decodedDisc.registrationNumber !== undefined) {
			discJson["RegistrationNumber"] = this._scannerService.decodedDisc.registrationNumber;
		}

		if (this._scannerService.decodedDisc.engineNumber !== null && this._scannerService.decodedDisc.engineNumber !== undefined) {
			discJson["EngineNumber"] = this._scannerService.decodedDisc.engineNumber;
		}

		if (this._scannerService.decodedDisc.licenseNumber !== null && this._scannerService.decodedDisc.licenseNumber !== undefined) {
			discJson["LicenseNumber"] = this._scannerService.decodedDisc.licenseNumber;
		}

		if (this._scannerService.decodedDisc.year !== null && this._scannerService.decodedDisc.year !== undefined) {
			discJson["Year"] = this._scannerService.decodedDisc.year;
		}

		return discJson;
	}

	private async ScanAnotherDisc(): Promise<void> {
		this.indicator.show(Config.loaderOptions);
		this.clearAllFields();
		this.onScan();
		this.indicator.hide();
	}

	private clearAllFields(): void {
		this._scannerService.decodedDisc.make = "";
		this._scannerService.decodedDisc.model = "";
		this._scannerService.decodedDisc.variant = "";
		this._scannerService.decodedDisc.mmCode = "";
		this._scannerService.decodedDisc.engineNumber = "";
		this._scannerService.decodedDisc.vin = "";
		this._scannerService.decodedDisc.year = null;
		this._scannerService.decodedDisc.mileage = null;
		this._scannerService.decodedDisc.mileage = null;
		this._scannerService.decodedDisc.condition = "";
		this._scannerService.decodedDisc.retailPrice = null;
		this._scannerService.decodedDisc.tradePrice = null;
		this.disabledTu = false;
	}
}
