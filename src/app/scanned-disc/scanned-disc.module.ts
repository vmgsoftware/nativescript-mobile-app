import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { SpacePipeModule } from "~/app/shared/spacePipe.module";

import { NativeScriptFormsModule } from "nativescript-angular";
import { ScannedDiscRoutingModule } from "./scanned-disc-routing.module";
import { ScannedDiscComponent } from "./scanned-disc.component";

@NgModule({
	imports: [
		NativeScriptCommonModule,
		ScannedDiscRoutingModule,
		SpacePipeModule,
		NativeScriptFormsModule
	],
	declarations: [
		ScannedDiscComponent
	],
	schemas: [
		NO_ERRORS_SCHEMA
	]
})
export class ScannedDiscModule { }
