import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {tokenNotExpired} from "angular2-jwt";
import {Auth0} from "nativescript-auth0";
import * as appSettings from "tns-core-modules/application-settings";
import * as dialogs from "tns-core-modules/ui/dialogs";
import {action, alert} from "tns-core-modules/ui/dialogs";
import {Config} from "~/app/Config";
import {MmcodesRepository} from "~/app/DataLayer/repositories/mmcodes.repository";
import {SharedService} from "~/app/shared/shared.service";
import {RouterExtensions} from "nativescript-angular";
import {CarService} from "~/app/DataLayer/car.service";
import {LeadService} from "~/app/crm/shared/lead.service";
import {NotificationService} from "~/app/shared/notification.service";

@Injectable()
export class AuthService {
	private _auth0: Auth0;
	public transunionSubscriptionStatus: boolean = false;

	constructor(private _mmCodes: MmcodesRepository, private _sharedService: SharedService,
				public http: HttpClient, private router: Router, private _routerExtensions: RouterExtensions,
				private _carService: CarService, private _leadService: LeadService, private _notificationService: NotificationService) {
		this._auth0 = new Auth0("kPyUdCZAkNb0ZlIuJOVWLg8ePwygN2Ed", "vmg.eu.auth0.com");
	}

	getToken(): string {
		return appSettings.getString("accessToken", "0");
	}

	isAuthenticated(): boolean {
		const token = this.getToken();
		if (token !== "0") {
			return tokenNotExpired(null, token);
		}
		return true;
	}

	refreshToken() {
		this._auth0.renewCredentials(appSettings.getString("refreshToken")).then((res) => {
			appSettings.setString("accessToken", res.accessToken);
		}, (error) => {
			console.log(error);
			dialogs.alert("Auth0 error " + error);
		});
	}

	async singleSignOnLogIn(email: string, pass: string) {
		const userInfo = {
			emailAddress: email,
			password: pass
		};
		const headers: HttpHeaders = new HttpHeaders({
			"Content-Type": "application/json"
		});
		await this.http.post(Config.loginUrl + "/api/user/login", userInfo, {
			headers,
			observe: "response"
		})
			.toPromise()
			.then(async (response: HttpResponse<any>) => {
					if (response.body.statusCode === 200) {
						appSettings.setString("accessToken", response.body.data.accessToken);
						appSettings.setString("refreshToken", response.body.data.refreshToken);
						appSettings.setString("emailAddress", userInfo.emailAddress);
						const headers: HttpHeaders = new HttpHeaders({
							"Content-Type": "application/json"
						});
						await this.http.get(Config.ApiUrl + "/api/users/profile", {
							headers,
							observe: "response"
						})
							.toPromise()
							.then(async (response: HttpResponse<any>) => {
									if (response.body.statusCode === 200) {
										if (response.body.data.isLockedOut === false) {
											if (response.body.data.companies.length > 1) {
												let companyNames = [];
												for (let company of  response.body.data.companies) {
													companyNames.push(company.name);
												}
												let options = {
													title: "Company selection",
													message: "Select Company Profile",
													cancelButtonText: "Cancel",
													actions: companyNames
												};

												await action(options).then(async (result) => {
													if (result === "Cancel") {
														//Log out
														await this.logOutUser();
													}
													else {
														let index = companyNames.indexOf(result);


														try {
															appSettings.setString("companyId", response.body.data.companies[index].companyId.toString());
															appSettings.setString("companyName", response.body.data.companies[index].name);
															appSettings.setString("CurrentBranchGuid", response.body.data.companies[index].branchId);
														}catch{
															await AuthService.contactSupport("No company assigned to user");
															await this.logOutUser();

														}

													}

												});
											} else {
												try{
													appSettings.setString("companyId", response.body.data.companies[0].companyId.toString());
													appSettings.setString("companyName", response.body.data.companies[0].name);
													appSettings.setString("CurrentBranchGuid", response.body.data.companies[0].branchId);
												}catch{
													await AuthService.contactSupport("No company assigned to user");
													await this.logOutUser();
												}

											}
											if (response.body.data.userSecurityRoles.length === 0){
												await AuthService.contactSupport("User Profile does not contain any permissions");
												//Log out
												this.logOutUser();
											}

											let userSecurityRoles = response.body.data.userSecurityRoles.filter(function (el) {
												return el.branchGuid == appSettings.getString("CurrentBranchGuid");
											});

											await this.http.get(Config.ApiUrl + "/api/scanneddisks/transunion/status", {
												headers,
												observe: "response"
											})
												.toPromise()
												.then( async (response: HttpResponse<any>) =>{
													if (response.body.statusCode === 200)
														this.transunionSubscriptionStatus = response.body.data == true;
													else
													await AuthService.contactSupport("Error getting TransUnion subscription status");
												});

											appSettings.setBoolean("canViewStock", userSecurityRoles[0].canViewStock as boolean);
											appSettings.setBoolean("canAddEditStock", userSecurityRoles[0].canAddEditStock as boolean);
											appSettings.setBoolean("canViewPurchasePrice", userSecurityRoles[0].canViewPurchasePrice as boolean);
											appSettings.setBoolean("canViewLeads", userSecurityRoles[0].canViewLeads as boolean);
											appSettings.setBoolean("canAddEditLeads", userSecurityRoles[0].canAddEditLeads as boolean);
											appSettings.setBoolean("canDeleteLeads", userSecurityRoles[0].canDeleteLeads as boolean);
											appSettings.setBoolean("canScanDisks", userSecurityRoles[0].canScanDisks as boolean);
											appSettings.setBoolean("canUseTransunion", userSecurityRoles[0].canUseTransunion as boolean);
											appSettings.setBoolean("transunionSubscriptionStatus", this.transunionSubscriptionStatus as boolean);


											appSettings.setString("userId", response.body.data.userId);
											appSettings.setString("userEmail", response.body.data.emailAddress);
											await this._mmCodes.setupMMCodes(appSettings.getString("mmcodesdatedsince", "0"));
											await this._carService.getCars();
											await this._leadService.GetLeads();
											await this._sharedService.getAllVehicleCategories();
										} else {
											await AuthService.contactSupport("Profile is locked");
											//logging out if the user profile is locked
											await this.logOutUser();

										}

									}
								}
								, async (error) => {
									console.log("ERROR profile ", error);
									//logging out if the user profile fails
									await this.logOutUser();
									await AuthService.contactSupport("Service unavailable");
								});
					} else {
						await AuthService.contactSupport("Service unavailable");
					}
				}
				, async (error) => {
				await this._notificationService.errorToastMessage("Login Error", "Invalid Credentials");
				});
	}

	async setUpCodes() {
		await this._mmCodes.setupMMCodes(appSettings.getString("mmcodesdatedsince", "0"));
	}

	private async logOutUser() {
		let mmcodes = appSettings.getString("mmcodesdatedsince", "0");
		appSettings.clear();
		appSettings.setString("mmcodesdatedsince", mmcodes);
		await this._sharedService.clearAllTablesExceptMmcodes();
		this._sharedService.homeLoading = false;
		return;
	}

	private static async contactSupport(error) {
		await alert(` ${error}, please contact support on 0877026300`);
	}
}
