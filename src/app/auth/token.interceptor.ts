import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {AuthService} from "./auth.service";
import {SharedService} from "~/app/shared/shared.service";
import * as appSettings from "tns-core-modules/application-settings";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
	isOnline: boolean = false;

	constructor(public _authService: AuthService, private _sharedService: SharedService) {

	}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		if (this._authService.isAuthenticated()) {
			console.log("AUTHENTIC!");
		} else {
			console.log("EXPIRED!");
			this._authService.refreshToken();

		}

		if (this._authService.getToken() !== "0") {
			if (appSettings.getString("CurrentBranchGuid", "empty") !== "empty")
			{
				request = request.clone({
					setHeaders: {
						Authorization: `Bearer ${this._authService.getToken()}`,

						CurrentBranchGuid: appSettings.getString("CurrentBranchGuid")
					}
				});
			}
			else {
				request = request.clone({
					setHeaders: {
						Authorization: `Bearer ${this._authService.getToken()}`
					}
				});
			}

		}

		return next.handle(request);
	}
}
