import { Mode, OptionsCommon } from "@nstudio/nativescript-loading-indicator";
import { Car } from "~/app/DataLayer/Entities/Car";
import { CarImage } from "~/app/DataLayer/Entities/CarImage";
import { Lead } from "~/app/DataLayer/Entities/Lead";
import { MMCode } from "~/app/DataLayer/Entities/MMCode";
import { ScannedDisc } from "~/app/DataLayer/Entities/ScannedDisc";
import { UserProfileModel } from "~/app/user-profile.model";

export class Config {
	public static Entities: Array<any> = [MMCode, ScannedDisc, Lead, Car, CarImage];
	public static DatabaseName: string = "my.db";
	public static ApiUrl: string;
	public static transunionUrl: string;
	public static loginUrl: string;
	public static carplaceBrochureUrl: string = "https://thecarplace.co.za/brochure/";
	public static appVersion: string = "Version 1.15.00";
	public static userProfile: UserProfileModel;
	public static loaderOptions: OptionsCommon = {
		message: "Loading...",
		details: "Please wait.",
		// progress: 0.65,
		margin: 10,
		dimBackground: true,
		color: "#448744", // color of indicator and labels
		// background box around indicator
		// hideBezel will override this if true
		backgroundColor: "#ffffff",
		userInteractionEnabled: false, // default true. Set false so that the touches will fall through it.
		hideBezel: false, // default false, can hide the surrounding bezel
		mode: Mode.Text, // see options below
		android: {
			cancelable: false
		},
		ios: {
			square: true
		}
	};
}
