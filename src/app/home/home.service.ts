import {Injectable, NgZone} from "@angular/core";
import {Observable, throwError} from "rxjs";

import { Config } from "~/app/Config";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { SharedService } from "~/app/shared/shared.service";

@Injectable({
	providedIn: "root"
})

export class HomeService {

	constructor(public http: HttpClient,) {
	}

	async ForgotPassword(emailAddress:object)
	{
		let headers: HttpHeaders = new HttpHeaders({
			"Content-Type": "application/json"
		});
		return await this.http.post(Config.loginUrl + "/api/user/resetpasswordviaemail/", emailAddress, {headers: headers, observe: 'response'})
			.toPromise()
			.then((response: any) => {
				return response.status;
			}, (error) => {
				console.log("error::: ", error);
				return error;
			});

	}

}
