import {AfterViewInit, Component, ElementRef, NgZone, OnInit, ViewChild} from "@angular/core";
import * as application from "application";
import { RouterExtensions } from "nativescript-angular";
import { exit } from "nativescript-exit";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as appSettings from "tns-core-modules/application-settings";
import { isIOS } from "tns-core-modules/platform";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { Color, isAndroid, Page } from "tns-core-modules/ui/page";
import { TextField } from "tns-core-modules/ui/text-field";
import * as utils from "utils/utils";
import { AuthService } from "~/app/auth/auth.service";
import { MmcodesRepository } from "~/app/DataLayer/repositories/mmcodes.repository";
import { HomeService } from "~/app/home/home.service";
import { SharedService } from "../shared/shared.service";
import { registerElement } from 'nativescript-angular/element-registry';
import { Gif } from 'nativescript-gif';

registerElement('Gif', () => Gif);

import {
	LoadingIndicator,
	Mode,
	OptionsCommon
} from "@nstudio/nativescript-loading-indicator";
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";
import { Config } from "~/app/Config";
import { NotificationService } from "~/app/shared/notification.service";

@Component({
	selector: "Home",
	moduleId: module.id,
	templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit, AfterViewInit {

	@ViewChild("label", {static: false}) public label: ElementRef;

	get needToLogIn(): boolean {
		if (appSettings.getString("accessToken", "0") === "0") {
			return true;
		} else {
			return false;
		}
	}

	get companyName(): string {
		return appSettings.getString("companyName");
	}

	get userEmail(): string {
		return appSettings.getString("userEmail");
	}

	private static exitApplication() {
		exit();
	}
	@ViewChild("passwordField", {static: false}) public passwordField: ElementRef;
	public email: string = "";
	public password: string = "";
	public showPasswordIcon: boolean = true;
	public indicator = new LoadingIndicator();
	public showroomTapped: boolean = false;
	public leadsTapped: boolean = false;
	public scannerTapped: boolean = false;
	public tuTapped: boolean = false;
	public supportTapped: boolean = false;

	constructor(public _sharedService: SharedService, private _mmcodesRepo: MmcodesRepository, private _authService: AuthService, private _ngZone: NgZone,
				         private routerExtensions: RouterExtensions, private page: Page, private _homeService: HomeService, private _notificationService: NotificationService) {
		if (isAndroid) {
			this.page.on(application.AndroidApplication.activityBackPressedEvent, HomeComponent.exitApplication, this);
			this.page.on(application.AndroidApplication.activityStoppedEvent, HomeComponent.exitApplication, this);
		}
	}

	/* This prevents the ios
	*  version from being able to
	*  swipe back to previous routes
	* */
	public ngAfterViewInit() {
		if (isIOS) {
			setTimeout(() => {
				this.page.frame.viewController.view.removeGestureRecognizer(this.page.frame.viewController.interactivePopGestureRecognizer);
			}, 200);
		}
	}

	public async ngOnInit() {
		await this.setupHomePage();
	}

	public toggleShow() {
		this.passwordField.nativeElement.secure = !this.passwordField.nativeElement.secure;
		this.showPasswordIcon = !!this.passwordField.nativeElement.secure;
	}

	public onDrawerButtonTap(): void {
		const sideDrawer = <RadSideDrawer>app.getRootView();
		sideDrawer.showDrawer();
	}

	public async navToShowroom() {
		this.showroomTapped = true;
		setTimeout(async () => {
			if (appSettings.getBoolean("canViewStock", false)) {
				this.indicator.show(Config.loaderOptions);
				await this._ngZone.run(async () => {
					await this.routerExtensions.navigate(["/showroom"], {
						transition: {
							name: "fade"
						}
					}).then(() => {
						setTimeout(async () => {
							this.indicator.hide();
						}, 500);
					});
				});
			} else {
				this.showroomTapped = false;
				alert("Oh no! it looks as though you need your permissions to be set in your VMG DMS to use this feature. Please ask the authorised person (Owner) to email the permission request to support@vmgsoftware.co.za. We’ll get you up and running in no time.");
			}
		}, 100);
	}

	public async navToCrm() {
		this.leadsTapped = true;
		setTimeout(async () => {
			if (appSettings.getBoolean("canViewLeads", false)) {
				this.indicator.show(Config.loaderOptions);
				await this._ngZone.run(async () => {
					await this.routerExtensions.navigate(["/crm"], {
						transition: {
							name: "fade"
						}
					}).then(() => {
						setTimeout(async () => {
							this.indicator.hide();
						}, 500);
					});
				});
			} else {
				this.leadsTapped = false;
				alert("Oh no! it looks as though you need your permissions to be set in your VMG DMS to use this feature. Please ask the authorised person (Owner) to email the permission request to support@vmgsoftware.co.za. We’ll get you up and running in no time.");
			}
		}, 100);
	}

	public async navToScanner() {
		this.scannerTapped = true;
		setTimeout(async () => {
			if (appSettings.getBoolean("canScanDisks", false)) {
				this.indicator.show(Config.loaderOptions);
				await this._ngZone.run(async () => {
					await this.routerExtensions.navigate(["/scanned-disc"], {
						transition: {
							name: "fade"
						}
					}).then(() => {
						setTimeout(async () => {
							this.indicator.hide();
						}, 500);
					});
				});
			} else {
				this.scannerTapped = false;
				alert("Oh no! it looks as though you need your permissions to be set in your VMG DMS to use this feature. Please ask the authorised person (Owner) to email the permission request to support@vmgsoftware.co.za. We’ll get you up and running in no time.");
			}
		}, 100);

	}

	public async navToTu() {
		this.tuTapped = true;
		setTimeout(async () => {
			if (appSettings.getBoolean("canUseTransunion", false) && appSettings.getBoolean("transunionSubscriptionStatus", false)) {
				this.indicator.show(Config.loaderOptions);
				await this._ngZone.run(async () => {
					await this.routerExtensions.navigate(["/transunion-pricing"], {
						transition: {
							name: "fade"
						}
					}).then(() => {
						setTimeout(async () => {
							this.indicator.hide();
						}, 500);
					});
				});
			} else if (appSettings.getBoolean("transunionSubscriptionStatus", false) === false) {
				this.tuTapped = false;
				dialogs.confirm({
					message: "Aw shucks! It looks as though your VMG TransUnion credentials are still missing. Please visit our blog page to find out how to register for this feature.",
					okButtonText: "Blog Page",
					cancelButtonText: "No Thanks"
				}).then((result) => {
					if (result == true) {
						utils.openUrl("https://www.vmgsoftware.co.za/post/0/Register-for-TU-Pricing-in-VMG");
					} else {
						return;
					}
				});
			} else {
				this.tuTapped = false;
				alert("Oh no! it looks as though you need your permissions to be set in your VMG DMS to use this feature. Please ask the authorised person (Owner) to email the permission request to support@vmgsoftware.co.za. We’ll get you up and running in no time.");
			}
		}, 100);
	}

	public async navToSupport() {
		this.supportTapped = true;
		setTimeout(async () => {
			this.indicator.show(Config.loaderOptions);
			await this._ngZone.run(async () => {
				await this.routerExtensions.navigate(["/support"], {
					transition: {
						name: "fade"
					}
				}).then(() => {
					setTimeout(async () => {
						this.indicator.hide();
					}, 500);
				});
			}, 100);
			});
	}

	public async submit() {
		if (!this.email || !this.password) {
			await this._notificationService.warningToastMessage("Please provide both an email address and password.", "");

			return;
		} else {
			this.logIn();
		}
	}

	public async logIn() {
		this._sharedService.homeLoading = true;
		await this._authService.singleSignOnLogIn(this.email, this.password);
		this._sharedService.homeLoading = false;
		if (appSettings.getString("accessToken", "0") !== "0") {
			await this._sharedService.registerForPushNotifications();
		}

	}

	public forgotPassword() {
		prompt({
			title: "Forgot Password",
			message: "\nEnter the email address you used to register for VMG Mobile to reset your password.",
			defaultText: "",
			okButtonText: "Ok",
			cancelButtonText: "Cancel"
		}).then(async (data) => {
			if (data.result) {
				const regexpEmail = new RegExp("[\\w-]+@([\\w-]+\\.)+[\\w-]+");
				const validEmail = regexpEmail.test(data.text);

				if (validEmail) {

					const email = {
						emailAddress: data.text
					};

					const result = await this._homeService.ForgotPassword(email);

					if (result.status == 200) {
						await alert({
							title: "VMG Mobile",
							message: "Your password change request was submitted.\n\nPlease check your email for instructions on choosing a new password.",
							okButtonText: "Ok"
						});
					} else if (result.status == 400) {
						await alert({
							title: "VMG Mobile",
							message: "Reset password not available, please contact support.",
							okButtonText: "Ok"
						});
					}
				} else {
					await alert({
						title: "VMG Mobile",
						message: "The email address provided is not valid. Please enter a valid email address.",
						okButtonText: "Ok"
					}).then(() => {
						this.forgotPassword();
					});
				}
			}
		});
	}

	private async setupHomePage() {
		this.email = appSettings.getString("userEmail", "");
		await this._sharedService.getTotalLeadsAndShowroom();
	}

	get version(): string {
		return Config.appVersion;
	}
}
