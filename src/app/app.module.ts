import { HttpClientModule } from "@angular/common/http";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { AuthService } from "~/app/auth/auth.service";
import { HomeComponent } from "~/app/home/home.component";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { TokenInterceptor } from "./auth/token.interceptor";

export function createBarcodeScanner() {
	return new BarcodeScanner();
}

@NgModule({
	bootstrap: [
		AppComponent
	],
	imports: [
		AppRoutingModule,
		NativeScriptModule,
		NativeScriptUISideDrawerModule,
		HttpClientModule
	],
	declarations: [
		AppComponent
	],
	schemas: [
		NO_ERRORS_SCHEMA
	],
	providers: [
		{provide: BarcodeScanner, useFactory: (createBarcodeScanner)},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: TokenInterceptor,
			multi: true
		},
		AuthService

	]
})
export class AppModule {
}
