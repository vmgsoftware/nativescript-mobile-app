import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Config } from "~/app/Config";
import { ScannedDisc } from "~/app/DataLayer/Entities/ScannedDisc";
import { GenericRepository } from "~/app/DataLayer/generic-repository";
import { ScannedDiscRepository } from "~/app/DataLayer/repositories/scanned-disc-repository";
import { StringHelper } from "~/app/shared/helpers/string-helper";
import { SharedService } from "~/app/shared/shared.service";
import {CarService} from "~/app/DataLayer/car.service";

@Injectable({
	providedIn: "root"
})
export class ScannerService {
	public offlineDiscs: Array<any>;
	public decodedDisc: ScannedDisc;

	constructor(public http: HttpClient, private _sharedService: SharedService, private _scannedDiscRepo: ScannedDiscRepository, private _carService: CarService,) {

	}

	public async SetupScanner() {
		this.offlineDiscs = await this.GetOfflineDiscs();
		if (this.offlineDiscs.length > 0) {
			await this.PostOfflineDiscs();
			alert("posted offline disc.");
		}
	}

	public async GetOfflineDiscs() {
		return this._scannedDiscRepo.getAllBySearch({where: [{recordLive: false}]});
	}

	public async PostOfflineDiscs() {
		for (const index in this.offlineDiscs) {
			console.log("posting offline disc...");
			await this.PostDisc(this.offlineDiscs[index]);
		}
	}

	public async PostDisc(disc: ScannedDisc) {
		let status = "";
		console.log(disc);
		try {
			const statusCode = await this.PostDiscJson(disc);
			if (Number(statusCode) === 201) {
				disc.recordLive = true;
				await this._scannedDiscRepo.upsert(disc);
				this._carService.getCars();
				status = "success";
			} else {
				status = "error";
			}
		} catch (error) {
			console.log("Failed to upload to stock error :::", error);
			status = "error";
		}

		return status;
	}

	public PostDiscJson(disc: any) {
		const headers: HttpHeaders = new HttpHeaders({
			"Content-Type": "application/json"
		});

		return this.http.post(Config.ApiUrl + "/api/scanneddisks", disc, {headers, observe: "response"})
			.toPromise()
			.then((response: any) => {
				return response.status;
			}, (error) => {
				console.log("error::: ", error);
				return error;
			});

	}

	public async getYearByVin(vin: string): Promise<number> {
		const headers: HttpHeaders = new HttpHeaders({
			"Content-Type": "application/json"
		});

		return this.http.get(Config.transunionUrl + "/transunion/getVehicleYear/" + vin, {headers, observe: "response"})
			.toPromise()
			.then((response: any) => {
				if (response.body.message.vehicleYears !== undefined && response.body.message.vehicleYears.length > 0) {
						return response.body.message.vehicleYears[0];
				} else {
					return null;
				}
			}, (error) => {
				console.log("error : ", error);

				return null;
			});

	}

	private handleErrors(error: Response): Observable<never> {
		return throwError(error);
	}
}
