export class Car {
	id: string;
	stockCode: string;
	make: string;
	model: string;
	year: number;
	mileage: number;
	colour: string;
	condition: string;
	purchasePrice: number;
	sellingPrice: number;
	imageUrls: Array<string>;
	dateCreated: string;
	dateUpdated: string;

	constructor(options: any) {
		this.colour = options.colour;
		this.stockCode = options.stockCode;
		this.condition = options.condition;
		this.dateCreated = options.dateCreated;
		this.dateUpdated = options.dateUpdated;
		this.make = options.make;
		this.mileage = options.mileage;
		this.sellingPrice = options.sellingPrice;
		this.purchasePrice = options.purchasePrice;
		this.imageUrls = options.imageUrls;
		this.year = options.year;
	}
}
