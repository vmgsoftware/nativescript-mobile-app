import { Component, ElementRef, NgZone, OnDestroy, OnInit, ViewChild } from "@angular/core";
import * as application from "application";
import { RouterExtensions } from "nativescript-angular/router";
import { ListViewEventData, LoadOnDemandListViewEventData, RadListView } from "nativescript-ui-listview";
import { Subscription } from "rxjs";

import { registerElement } from "nativescript-angular/element-registry";
import { isAndroid, isIOS } from "platform";
import { Color } from "tns-core-modules/color";
import { EventData } from "tns-core-modules/data/observable";
import { fromUrl, ImageSource } from "tns-core-modules/image-source";
import { Image } from "tns-core-modules/ui/image";
import { SearchBar } from "tns-core-modules/ui/search-bar";
import { Like } from "typeorm/browser";
import { Car } from "../../app/DataLayer/Entities/Car";
import { CarService } from "../DataLayer/car.service";

import {
	LoadingIndicator,
	Mode,
	OptionsCommon
} from "@nstudio/nativescript-loading-indicator";
import { Page } from "ui/page";
import { Config } from "~/app/Config";
import { CarRepository } from "~/app/DataLayer/repositories/car-repository";
import { NotificationService } from "~/app/shared/notification.service";
import { SharedService } from "~/app/shared/shared.service";
registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);

@Component({
	selector: "CarsList",
	moduleId: module.id,
	templateUrl: "./car-list.component.html",
	styleUrls: ["./car-list.component.scss"]
})
export class CarListComponent implements OnInit, OnDestroy {

	get cars(): Array<Car> {
		return this._cars;
	}

	get isLoading(): boolean {
		return this._isLoading;
	}

	private static orderCarsByDateUpdated(a, b) {
		if (a.dateUpdated < b.dateUpdated) {
			return 1;
		}
		if (a.dateUpdated > b.dateUpdated) {
			return -1;
		}
		return 0;
	}
	public indicator = new LoadingIndicator();
	public platformIOS = isIOS;
	public platformAndroid = isAndroid;
	public totalCars: number;
	public loadingMode: string = "None";

	public carsOnView: Array<Car> = new Array<Car>();
	public count: number = 10;
	public searchCounter: number = 10;
	public numberOfSearchResults: number;
	public isSearching: boolean = false;
	public notRefreshing: boolean = true;
	public searchTerm: string = "";
	public firstBatchOfCars: Array<Car>;
	private _isLoading: boolean = false;
	private _cars: Array<Car> = new Array<Car>();
	private _dataSubscription: Subscription;
	private loading: boolean = false;

	constructor(private _carService: CarService, private _routerExtensions: RouterExtensions, private page: Page, private _ngZone: NgZone, private _sharedService: SharedService,
				         private _carRepo: CarRepository, private _notificationService: NotificationService) {
		if (isAndroid) {
			this.page.on(application.AndroidApplication.activityBackPressedEvent, this.onBackButtonTap, this);
		}
	}

	public async ngOnInit() {
		this._carService.getCars();
		this.carsOnView = [];
		this.firstBatchOfCars = await this._carRepo.getAllPaged(0, 10);
		this.firstBatchOfCars = this.firstBatchOfCars.sort(CarListComponent.orderCarsByDateUpdated);

		this.carsOnView.push(...this.firstBatchOfCars);
		this.totalCars = await this._carRepo.getTotal();

	}

	public ngOnDestroy(): void {
		if (this._dataSubscription) {
			this._dataSubscription.unsubscribe();
			this._dataSubscription = null;
		}
	}

	public async onCarItemTap(args: ListViewEventData) {
		const tappedCarItem = args.view.bindingContext;
		this.indicator.show(Config.loaderOptions);
		await this._routerExtensions.navigate(["/showroom/car-detail",
			tappedCarItem.id
		], {
			transition: {
				name: "slide"
			}
		}).then(() => {
			setTimeout(async () => {
				this.indicator.hide();
			}, 500);
		});
	}

	public GetTappedCar(id: string): Car {
		for (const index in this._cars) {
			if (this._cars[index].id === id) {
				return this._cars[index];
			}
		}
	}

	public async onFilterTextChanged(args) {
		const searchBar = <SearchBar>args.object;

		this.isSearching = true;
		this.searchTerm = searchBar.text;
		this.carsOnView = await this._carRepo.getAllBySearch({
			where: [{stockCode: Like("%" + searchBar.text + "%")}, {make: Like("%" + searchBar.text + "%")}, {model: Like("%" + searchBar.text + "%")}
				, {year: Like("%" + searchBar.text + "%")}, {colour: Like("%" + searchBar.text + "%")}]
			, skip: 0,
			take: 10
		});
		const getSearchCountObj = await this._carRepo.getSearchCount({
			where: [{stockCode: Like("%" + this.searchTerm + "%")}, {make: Like("%" + this.searchTerm + "%")}, {model: Like("%" + this.searchTerm + "%")}
				, {year: Like("%" + this.searchTerm + "%")}, {colour: Like("%" + this.searchTerm + "%")}]
		});
		this.numberOfSearchResults = getSearchCountObj[1];
		if (searchBar.text.length > 0) {
			this.isSearching = true;
		} else {
			this.carsOnView = [];
			this.firstBatchOfCars = [];
			const firstBatchOfCars = await this._carRepo.getAllPaged(0, 10);
			this.carsOnView.push(...firstBatchOfCars);
			this.totalCars = await this._carRepo.getTotal();
			this.count = 10;
			this.isSearching = false;
		}

	}

	public removeSearchFocus(args) {
		const searchBar: SearchBar = <SearchBar>args.object;
		if (isAndroid) {
			searchBar.android.clearFocus();
		}
	}

	public onClear(args) {
		args.text = "";
		args.dismissSoftInput();
		this.loadingMode = "Manual";
	}

	public onVehiclesLoading(args: ListViewEventData) {
		if (args.index % 2 === 0) {
			args.view.backgroundColor = new Color("#eee");
		} else {
			args.view.backgroundColor = new Color("#e6e6e6");
		}
	}

	public async onBackButtonTap() {
		await this._ngZone.run(async () => {
			this.indicator.show(Config.loaderOptions);
			await this._routerExtensions.navigateByUrl("/home").then(() => {
				setTimeout(async () => {
					this.indicator.hide();
				}, 500);
			});
		});

	}

	public async onLoadMoreItemsRequested(args: LoadOnDemandListViewEventData) {
		if (!this.isSearching) {
			if (this.totalCars > this.count) {
				const batchOfCars = await this._carRepo.getAllPaged(this.count, 10);
				this.carsOnView.push(...batchOfCars.sort(CarListComponent.orderCarsByDateUpdated));
				this.count += 10;
			} else {
				return;
			}
		} else {
			if (this.numberOfSearchResults > this.searchCounter) {
				const batchOfCars = await this._carRepo.getAllBySearch({
					where: [{stockCode: Like("%" + this.searchTerm + "%")}, {make: Like("%" + this.searchTerm + "%")}, {model: Like("%" + this.searchTerm + "%")}
						, {year: Like("%" + this.searchTerm + "%")}, {colour: Like("%" + this.searchTerm + "%")}]
					, skip: this.searchCounter,
					take: 10
				});
				this.carsOnView.push(...batchOfCars);
				this.searchCounter += 10;
			} else {
				// await this._notificationService.infoToastMessage("All vehicles matching the search has been loaded.", "");
				return;
			}
		}
	}

	public hideLoadMoreButton(listView: any) {
		if (this.carsOnView.length >= this.totalCars) {
			listView.notifyLoadOnDemandFinished();
			this.loadingMode = "None";
		}
	}

	public onSubmit(args) {
		const searchBar = <SearchBar>args.object;
		searchBar.dismissSoftInput();
	}

	public async onImageLoaded(args: EventData, url: string, myCar: Car) {
		fromUrl(url)
			.then(function(res: ImageSource) {
				// do nothing if image loads successfully
			}).catch((err) => {
			const myImg = <Image>args.object;
			myImg.src = "~/images/imagenotfound.png";
			const obj = this.carsOnView.find((o, i) => {
				if (o) {
					if (o.id === myCar.id) {
						if (myCar.carImages[0] !== undefined && myCar.carImages[0].imageUrl !== undefined) {
							myCar.carImages[0].imageUrl = "~/images/imagenotfound.png";
							this.carsOnView[i] = myCar;

							return true; // stop searching
						}

					}
				}

			});
		});
	}

	public async refreshList(args) {
		this._isLoading = true;
		const pullRefresh = args.object;
		await this._carService.getCars();
		this.carsOnView = [];
		this.firstBatchOfCars = await this._carRepo.getAllPaged(0, 10);
		this.firstBatchOfCars = this.firstBatchOfCars.sort(CarListComponent.orderCarsByDateUpdated);

		this.carsOnView.push(...this.firstBatchOfCars);
		this.totalCars = await this._carRepo.getTotal();
		this.count = 10;
		this.searchCounter = 10;
		setTimeout(() => {
			pullRefresh.refreshing = false;
			this._isLoading = false;
		}, 500);
	}
}
