import {Component, ElementRef, NgZone, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {
	LoadingIndicator
} from "@nstudio/nativescript-loading-indicator";
import {PageRoute, RouterExtensions} from "nativescript-angular/router";
import {Feedback} from "nativescript-feedback";
import * as SocialShare from "nativescript-social-share";
import * as appSettings from "tns-core-modules/application-settings";
import {fromUrl, ImageSource} from "tns-core-modules/image-source";
import * as dialogs from "tns-core-modules/ui/dialogs";
import {Color, EventData, isAndroid, isIOS} from "tns-core-modules/ui/frame";
import {ListPicker} from "tns-core-modules/ui/list-picker";
import {ScrollEventData, ScrollView} from "ui/scroll-view";
import {Config} from "~/app/Config";
import {LeadService} from "~/app/crm/shared/lead.service";
import {Car} from "~/app/DataLayer/Entities/Car";
import {CarImage} from "~/app/DataLayer/Entities/CarImage";
import {Lead} from "~/app/DataLayer/Entities/Lead";
import {StringHelper} from "~/app/shared/helpers/string-helper";
import {NotificationService} from "~/app/shared/notification.service";
import {SharedService} from "~/app/shared/shared.service";
import {CarService} from "../../DataLayer/car.service";

@Component({
	selector: "CarDetail",
	moduleId: module.id,
	templateUrl: "./car-detail.component.html",
	styleUrls: ["./car-detail.component.scss"]
})
export class CarDetailComponent implements OnInit {

	get allLeads() {
		this.leads = [];
		if (this.availableLeads !== undefined && this.availableLeads !== null) {
			for (const lead of this.availableLeads) {
				this.leads.push(`${lead.firstname} ${lead.lastname} | ${lead.emailAddress}`);
			}

			return this.leads;
		} else {
			return [];
		}

	}

	@ViewChild("myfilter", {static: false}) public myfilter: ElementRef;

	public _car: Car = new Car();
	public myAspec: string = "aspectFill";
	public editColour: string = "lightgray";
	public leads: Array<string>;
	public availableLeads: Array<Lead>;
	public platformIOS = isIOS;
	public platformAndroid = isAndroid;
	public indicator = new LoadingIndicator();
	private _editable: boolean = false;
	private loading: boolean;
	private feedback: Feedback;

	constructor(private _carService: CarService, private _pageRoute: PageRoute,
				private _routerExtensions: RouterExtensions, private _sharedService: SharedService, private _leadService: LeadService, private activatedRoute: ActivatedRoute,
				private _notificationService: NotificationService, private _ngZone: NgZone) {
		this.loading = true;
		this.feedback = new Feedback();
	}

	public async ngOnInit() {
		// if statement checks to see if user is coming from push notification.
		// local db only needs to be synced up if user is coming from a push notification.
		if (this.activatedRoute.toString().includes("push")) {
			await this._carService.getCars();
		}
		let id: string;
		id = this.activatedRoute.snapshot.paramMap.get("id");

		await this._carService.getCars().then(async () => {
			this._car = await this._carService.getCarUniqueId(id);

			this._carService.setSelectedCar(this._car);

			if (this._car.carImages.length > 0) {
				for (const carImage of this._car.carImages) {
					fromUrl(carImage.imageUrl)
						.then((res: ImageSource) => {
							// do nothing if image loads successfully

						}).catch((err) => {
						// Replace image Url if a error occurs while loading it
						carImage.imageUrl = "~/images/imagenotfound.png";
					});
				}
			} else {
				const carImage: CarImage = new CarImage();
				carImage.imageUrl = "~/images/imagenotfound.png";

				this._car.carImages.push(carImage);
			}

			if (appSettings.getBoolean("canAddEditLeads")) {
				this.availableLeads = [];
				this.availableLeads = await this._leadService.GetLeads();
			}

			this.loading = false;
		});

	}

	public onTap(args: EventData) {

		if (this.myAspec === "aspectFill") {
			this.myAspec = "aspectFit";
		} else if (this.myAspec === "aspectFit") {
			this.myAspec = "aspectFill";
		}
	}

	public onScroll(args: ScrollEventData) {
		// console.log("scrollX: " + args.scrollX + "; scrollY: " + args.scrollY);
	}

	public onScrollLoaded(args) {
		// scroll to specific position of the horizontal scroll list
		const scrollOffset = 330;
		(<ScrollView>args.object).scrollToHorizontalOffset(scrollOffset, true);
	}

	public async onBackButtonTap() {
		// backToPreviousPage() is used to prevent car-list ngOnInit from firing.
		this.indicator.show(Config.loaderOptions);
		await this._routerExtensions.backToPreviousPage();
		setTimeout(async () => {
			this.indicator.hide();
		}, 500);
	}

	public edit(): void {
		this._car.make = "";
		this._editable = true;
		this.editColour = "";
	}

	public updateVehicle(): void {
		this._carService.update(this._car);
		this._routerExtensions.backToPreviousPage();
	}

	public selectedMakeChanged(args) {
		const picker = <ListPicker>args.object;
		this._car.make = this._sharedService.makes[picker.selectedIndex];
		this._sharedService.fetchModel(this._car.make);
	}

	public async linkToLead() {
		if (this.allLeads.length > 0) {
			if (appSettings.getBoolean("canAddEditLeads")) {
				this.myfilter.nativeElement.show();
			} else {
				alert("You don't have permission to edit leads.");
			}
		} else {
			await this.feedback.info({
				title: "No Leads Found",
				message: "Tap here to add a lead.",
				titleColor: new Color("white"),
				backgroundColor: new Color("#263238"),
				duration: 5000,
				onTap: async () => {
					this.indicator.show(Config.loaderOptions);
					await this._ngZone.run(async () => {
						await this._routerExtensions.navigate(["crm/add-lead"]).then(() => {
							setTimeout(async () => {
								this.indicator.hide();
							}, 500);
						});
					});
				}
			});
		}
	}

	public async linkToNewLead() {
		if (appSettings.getBoolean("canAddEditLeads")) {
			await this.indicator.show(Config.loaderOptions);
			await this._ngZone.run(async () => {
				await this._routerExtensions.navigate(["crm/add-lead"], {
						animated: true, transition: {
						name: "slide", duration: 200, curve: "ease"
					}
				}).then(() => {
					setTimeout(async () => {
						this.indicator.hide();
					}, 500);
				});
			});
		} else {
			alert("You don't have permission to add new leads.");
		}
	}

	public share() {
		dialogs.action({
			message: "Share vehicle or assign to lead?",
			cancelButtonText: "Cancel",
			actions: ["Share", "Assign To Lead", "Add to new lead"]
		}).then(async (result) => {
			if (result === "Assign To Lead") {
				await this.linkToLead();
			} else if (result === "Add to new lead") {
				await this.linkToNewLead();
			} else if (result === "Share") {
				SocialShare.shareText(Config.carplaceBrochureUrl + appSettings.getString("companyId") + "/" + this._car.dmsStockId);
			}
		});
	}

	public async itemTapped(args) {
		const index = this.leads.indexOf(args.selectedItem);
		const leadToUpdate: Lead = this.availableLeads[index];
		if (!StringHelper.isNullOrEmpty(leadToUpdate.make)) {
			dialogs.confirm({
				title: "Assign Vehicle To lead",
				message: "\nThis lead has a vehicle assigned to it already, would you like to replace it?",
				okButtonText: "Yes",
				cancelButtonText: "No"
			}).then(async (result) => {
				if (result === true) {
					await this.assignVehicleToLead(leadToUpdate);
				}

			});
		} else {
			await this.assignVehicleToLead(leadToUpdate);
		}
	}

	private async assignVehicleToLead(leadToUpdate: Lead): Promise<void> {
		if (!StringHelper.isNullOrEmpty(this._car.mmCode)) {
			leadToUpdate.make = this._car.mmCode;
			leadToUpdate.model = this._car.mmCode;
			leadToUpdate.variant = this._car.mmCode;
			leadToUpdate.year = this._car.year;
			leadToUpdate.colour = this._car.colour;
			leadToUpdate.price = this._car.sellingPrice;
			leadToUpdate.mobileStockId = this._car.id;
			const resp = await this._leadService.UpdateLead(leadToUpdate);
			if (resp === 200) {
				await this._notificationService.successToastMessage("Link Successful", `Lead ${leadToUpdate.firstname} ${leadToUpdate.lastname} linked to ${this._car.make} ${this._car.model}`);
			} else {
				await this._notificationService.errorToastMessage("Link Failed", "Could not link vehicle with lead. Please contact support.");
			}
		} else {
			await this._notificationService.errorToastMessage("Link Failed, MMCODE not found.", "Could not link vehicle with lead. Please contact support.");
		}
		this._leadService.GetLeads();

	}
}
