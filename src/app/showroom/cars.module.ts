import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { SpacePipeModule } from "~/app/shared/spacePipe.module";

import { CarDetailComponent } from "./car-detail/car-detail.component";
import { CarListComponent } from "./car-list.component";
import { CarsRoutingModule } from "./cars-routing.module";

@NgModule({
	imports: [
		CarsRoutingModule,
		NativeScriptCommonModule,
		NativeScriptFormsModule,
		NativeScriptUIListViewModule,
		SpacePipeModule
	],
	declarations: [
		CarListComponent,
		CarDetailComponent
		// CarDetailEditComponent,
		// MyListSelectorComponent,
		// MyListSelectorModalViewComponent,
		// MyImageAddRemoveComponent
	],
	// entryComponents: [
	//     MyListSelectorModalViewComponent
	// ],
	providers: [],
	schemas: [
		NO_ERRORS_SCHEMA
	]
})
export class CarsModule { }
