import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { Http } from "@angular/http";
import * as application from "application";
import * as appSettings from "application-settings";
import { RouterExtensions } from "nativescript-angular";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import {isAndroid, isIOS} from "tns-core-modules/platform";
import * as utils from "tns-core-modules/utils/utils";
import { Config } from "~/app/Config";
import { EmailService } from "~/app/shared/email.service";
import {Page} from "ui/page";
import { SharedService } from "~/app/shared/shared.service";
import {LoadingIndicator} from "@nstudio/nativescript-loading-indicator";
// import {
// 	NSPhoneEventEmitter,
// 	sms,
// 	dial,
// 	requestCallPermission,
// 	SMSEvents,
// 	DialEvents
//   } from 'nativescript-phone';


@Component({
	selector: "ConnectionError",
	moduleId: module.id,
	templateUrl: "./support.html",
	styleUrls: ["./support.component.scss"]
})

export class SupportComponent implements OnInit {
	public platformIOS = isIOS;
	public platformAndroid = isAndroid;
	public indicator = new LoadingIndicator();

	constructor(private  _routerExtensions: RouterExtensions, private _emailService: EmailService, private page: Page, public _sharedService: SharedService, private http: HttpClient, private routerExtensions: RouterExtensions) {
		if(isAndroid)
			this.page.on(application.AndroidApplication.activityBackPressedEvent, this.onBackButtonTap, this);
	}

	ngOnInit() {
	}

	phoneSupport() {
		var phone = require('nativescript-phone');
		phone.dial('087-702-6300', false);
	}

	async sendEmail() {
		await this._emailService.sendEmail("support@vmgsoftware.co.za");
	}

	async reloadApplication() {
		this._sharedService.homeLoading = true;
		const headers: HttpHeaders = new HttpHeaders({
			"Content-Type": "application/json"
		});
		await this.http.put(Config.ApiUrl + "/api/users/unregisterdeviceid", {
			headers,
			observe: "response"
		}).toPromise();
		this._sharedService.totalLeads = 0;
		this._sharedService.totalVehicles = 0;
		this.indicator.show(Config.loaderOptions);
		await this.routerExtensions.navigate(["/home"], {
			transition: {
				name: "fade"
			}
		}).then(() => {
			this.indicator.hide();
		});
		appSettings.clear();
		await this._sharedService.clearAllTables();
		this._sharedService.homeLoading = false;
	}

	async onBackButtonTap() {
		this.indicator.show(Config.loaderOptions);
			await this._routerExtensions.navigateByUrl("/home").then(() => {
				setTimeout(async () => {
					this.indicator.hide();
				}, 500);
			});
	}

	public openPrivacyPolicy(): void {
		utils.openUrl("http://vmgsoftware.co.za/privacy-policy");
	}

	get version(): string {
		return Config.appVersion;
	}
}
