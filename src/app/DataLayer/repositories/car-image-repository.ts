import "reflect-metadata";

import { Injectable}  from "@angular/core";
import { GenericRepository } from "~/app/DataLayer/generic-repository";
import { CarImage } from "~/app/DataLayer/Entities/CarImage";


@Injectable({
	providedIn: "root"
})

export class CarImageRepository extends GenericRepository<CarImage>{


	public constructor() {
		super(CarImage);

	}


}
