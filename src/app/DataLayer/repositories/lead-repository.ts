import "reflect-metadata";

import {Injectable} from "@angular/core";
import {GenericRepository} from "~/app/DataLayer/generic-repository";
import {Lead} from "~/app/DataLayer/Entities/Lead";

@Injectable({
	providedIn: "root"
})

export class LeadRepository extends GenericRepository<Lead>{


	public constructor() {
		super(Lead);

	}


}
