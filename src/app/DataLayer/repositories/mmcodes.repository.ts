import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import * as appSettings from "tns-core-modules/application-settings";
import { Like } from "typeorm/browser";
import { Config } from "../../Config";
import { MMCode } from "../Entities/MMCode";
import { GenericRepository } from "../generic-repository";
import {StringHelper} from "~/app/shared/helpers/string-helper";

@Injectable({
	providedIn: "root"
})

export class MmcodesRepository extends GenericRepository<MMCode> {

	public categories: Array<string> = [];
	public makes: Array<string> = [];
	public models: Array<string> = [];
	public masterModels: Array<string> = [];
	public variants: Array<string> = [];

	public vehicleCategory: string;

	constructor(public http: HttpClient) {
		super(MMCode);
	}

	public async setupMMCodes(sinceDate: string): Promise<void> {
		const currMonth = new Date().getMonth();
		appSettings.setNumber("monthUpdatedLast", currMonth);
		const res: Array<MMCode> = await this.getMMCodesOnline(sinceDate);
		if (res !== null) {
			try {
				await this.batchUpsert(res);
			} catch (e) {
				await alert(e);
			}

		}
		await this.populateMakes();

	}

	public async batchUpsert(mmcodes: Array<MMCode>): Promise<void> {
		try {
			await super.upsert(mmcodes);
		} catch (e) {
			console.log("ERROR ::: ", e);
			await alert(e);
		}
	}

	public async getMMCodesOnline(sinceDate: string): Promise<Array<MMCode>> {
		let requestString = "";
		if (sinceDate === "0") {
			requestString = "mmcodes";
		} else {
			requestString = "mmcodes/updatedsince/" + sinceDate;
		}
		const headers: HttpHeaders = new HttpHeaders({
			"Content-Type": "application/json"
		});

		return this.http.get(Config.ApiUrl + "/api/" + requestString, {headers}).toPromise()
			.then((r: any) => {
				const unixTime = Math.round((new Date()).getTime() / 1000);
				appSettings.setString("mmcodesdatedsince", unixTime.toString());

				return r.data as Array<MMCode>;

			}, async (e) => {
				console.log("Error getting mmcodes online: ", e);

				return null;
			});
	}

	public async populateMakes(): Promise<void> {
		const tempMake: Array<MMCode> = await super.getAllBySearch({
			select: ["make"],
			order: {
				make: "ASC"
			}

		});

		for (const make in tempMake) {
			if (this.makes.includes(tempMake[make].make.toString())) {
				continue;
			} else {
				this.makes.push(tempMake[make].make.toString());
			}
		}
		this.models = [];
		this.variants = [];
	}

	public async getCategories(): Promise<Array<string>> {
		this.categories = [];
		const tempCategories: Array<MMCode> = await super.getAllBySearch({
			select: ["category"],
			order: {
				category: "ASC"
			}

		});

		for (const category in tempCategories) {
			if (this.categories.includes(tempCategories[category].category)) {
				continue;
			} else {
				this.categories.push(tempCategories[category].category.toString());
			}
		}

		this.makes = [];
		this.masterModels = [];
		this.variants = [];

		return this.categories;
	}

	public async GetCategoryByMakeModelVariant(make: string, model: string, variant: string): Promise<string> {
		if (! StringHelper.isNullOrEmpty(variant)) {
			const vehicleCategory: MMCode = await super.getFirstBySearch({
				where: [{variant}],
				select: ["category"],
				order: {
					variant: "ASC"
				}
			});

			return vehicleCategory.category.toString();
		} else if (! StringHelper.isNullOrEmpty(model)) {
			const vehicleCategory: MMCode = await super.getFirstBySearch({
				where: [{masterModel: model}],
				select: ["category"],
				order: {
					model: "ASC"
				}
			});
			return vehicleCategory.category.toString();
		} else if (! StringHelper.isNullOrEmpty(make)) {
			const vehicleCategory: MMCode = await super.getFirstBySearch({
				where: [{make}],
				select: ["category"],
				order: {
					make: "ASC"
				}
			});

			return vehicleCategory.category.toString();
		} else {
			return "";
		}

	}

	public async getMakesByCategory(category: string, year: string = null): Promise<Array<string>> {
		let whereClause = {};

		if (year !== null) {
			whereClause = {
				year,
				category
			};
		} else {
			whereClause = {
				category
			};
		}
		this.makes = [];
		const tempMakes: Array<MMCode> = await super.getAllBySearch({
			...whereClause && { where: [whereClause] },
			select: ["make"],
			order: {
				make: "ASC"
			}

		});

		for (const make in tempMakes) {
			if (this.makes.includes(tempMakes[make].make)) {
				continue;
			} else {
				this.makes.push(tempMakes[make].make.toString());
			}
		}
		this.masterModels = [];
		this.variants = [];

		return this.makes;
	}

	public async getMasterModelsByMakeAndCategory(make: string, category: string): Promise<Array<string>> {
		this.masterModels = [];
		const tempMasterModels: Array<MMCode> = await super.getAllBySearch({
			where: [{make, category}],
			select: ["masterModel"],
			order: {
				masterModel: "ASC"
			}

		});

		for (const masterModel in tempMasterModels) {
			if (this.masterModels.includes(tempMasterModels[masterModel].masterModel)) {
				continue;
			} else {
				this.masterModels.push(tempMasterModels[masterModel].masterModel.toString());
			}
		}
		this.variants = [];

		return this.masterModels;
	}

	public async getMasterModelsByMakeAndYear(make: string, year: string): Promise<Array<string>> {
		this.masterModels = [];
		const tempMasterModels: Array<MMCode> = await super.getAllBySearch({
			where: [{make,
				        years: Like("%" + year + "%")}],
			select: ["masterModel"],
			order: {
				masterModel: "ASC"
			}
		});

		for (const masterModel in tempMasterModels) {
			if (this.masterModels.includes(tempMasterModels[masterModel].masterModel)) {
				continue;
			} else {
				this.masterModels.push(tempMasterModels[masterModel].masterModel.toString());
			}
		}
		this.variants = [];

		return this.masterModels;
	}

	public async getVariantsByMasterModel(make: string, masterModel: string): Promise<Array<string>> {
		this.variants = [];
		const tempVariants: Array<MMCode> = await super.getAllBySearch({
			where: [{
				make,
				masterModel
			}],
			select: ["variant"],
			order: {
				variant: "ASC"
			}

		});

		for (const variant in tempVariants) {
			if (this.variants.includes(tempVariants[variant].variant)) {
				continue;
			} else {
				this.variants.push(tempVariants[variant].variant.toString());
			}
		}

		return this.variants;
	}

	public async getVariantsByMasterModelAndYear(make: string, masterModel: string, year: string): Promise<Array<string>> {
		this.variants = [];
		const tempVariants: Array<MMCode> = await super.getAllBySearch({
			where: [{
				make,
				masterModel,
				years: Like("%" + year + "%")
			}],
			select: ["variant"],
			order: {
				variant: "ASC"
			}

		});

		for (const variant in tempVariants) {
			if (this.variants.includes(tempVariants[variant].variant)) {
				continue;
			} else {
				this.variants.push(tempVariants[variant].variant.toString());
			}
		}

		return this.variants;
	}

}
