import "reflect-metadata";

import {Injectable} from "@angular/core";
import {GenericRepository} from "~/app/DataLayer/generic-repository";
import {Car} from "~/app/DataLayer/Entities/Car";

@Injectable({
	providedIn: "root"
})

export class CarRepository extends GenericRepository<Car>{


	public constructor() {
		super(Car);

	}


}
