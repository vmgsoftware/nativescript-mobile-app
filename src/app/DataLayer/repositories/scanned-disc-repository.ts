import "reflect-metadata";

import {Injectable} from "@angular/core";
import {GenericRepository} from "~/app/DataLayer/generic-repository";
import {ScannedDisc} from "~/app/DataLayer/Entities/ScannedDisc";

@Injectable({
	providedIn: "root"
})

export class ScannedDiscRepository extends GenericRepository<ScannedDisc>{


	public constructor() {
		super(ScannedDisc);

	}


}
