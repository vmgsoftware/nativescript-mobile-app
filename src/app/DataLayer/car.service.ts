import { Injectable, NgZone } from "@angular/core";
import { Observable, throwError } from "rxjs";
import * as appSettings from "tns-core-modules/application-settings";
import { getFile, getImage, getJSON, getString, HttpResponse, request } from "tns-core-modules/http";

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { EventData } from "tns-core-modules/data/observable";
import { fromUrl, ImageSource } from "tns-core-modules/image-source";
import { isIOS } from "tns-core-modules/platform";
import { Image } from "tns-core-modules/ui/image";
import { Config } from "~/app/Config";
import { CarImage } from "~/app/DataLayer/Entities/CarImage";
import { CarImageRepository } from "~/app/DataLayer/repositories/car-image-repository";
import { CarRepository } from "~/app/DataLayer/repositories/car-repository";
import { Car } from "./Entities/Car";

/* ***********************************************************
* This is the master detail data service. It handles all the data operations
* of retrieving and updating the data. In this case, it is connected to Firebase and
* is using the {N} Firebase plugin. Learn more about it here:
* https://github.com/EddyVerbruggen/nativescript-plugin-firebase
* The {N} Firebase plugin needs some initialization steps before the app starts.
* Check out how it is imported in the main.ts file and the actuFal script in /shared/firebase.common.ts file.
*************************************************************/
@Injectable({
	providedIn: "root"
})
export class CarService {
	public selectedCar: Car = new Car();

	constructor(public http: HttpClient, private _carRepo: CarRepository, private _carImageRepo: CarImageRepository) {

	}

	public async getCarUniqueId(id: string): Promise<Car> {
		return this._carRepo.getFirstBySearch({where: [{id}]});
	}


	public setSelectedCar(car: Car) {
		this.selectedCar = car;
	}

	public async getCars() {
		appSettings.setBoolean("doneCarCheck", true);
		let cars: Array<Car>;
		if (appSettings.getString("stockupdatedsince", "0") === "0") {
				cars = await this.loadCars();
				if (cars === null) {
					await this._carRepo.upsert(cars);

					return this._carRepo.getAll();
				}
		} else {
			const carsUpdated: Array<Car> = await this.loadCarsUpdatedSince(appSettings.getString("stockupdatedsince")) as Array<Car>;
			if (carsUpdated !== null) {
				await this._carRepo.bulkUpsertById(carsUpdated);
				await this._carRepo.bulkRemove({deleted: true});
				await this._carRepo.getAll();

				return;
			} else {
				await this._carRepo.getAll();

				return;
			}
		}

		const carsWithImages = new Array<Car>();
		for (const car of cars) {
			const carImages = new Array<CarImage>();
			for (const image of car.imageUrls) {
				const carImage = new CarImage();
				carImage.imageUrl = image;
				carImages.push(carImage);
			}
			car.carImages = carImages;

			carsWithImages.push(car);
		}

		await this._carImageRepo.removeAll();
		await this._carRepo.removeAll();
		await this.batchUpsert(carsWithImages);
		cars =  await this._carRepo.getAll();

		return cars;

	}

	public async batchUpsert(carsWithImages: Array<Car>) {
		const cars = [];
		while (carsWithImages.length) {
			cars.push(carsWithImages.splice(0, 100));
		}

		for (const car of cars) {
			await this._carRepo.upsert(car);
		}
	}

	public async loadCars(): Promise<Array<Car>> {
		 const headers: HttpHeaders = new HttpHeaders({
			 "Content-Type": "application/json"
		 });

		 return this.http.get(Config.ApiUrl + "/api/stock", {headers})
			.toPromise()
			.then((value: any) => {
				const unixTime = Math.round((new Date()).getTime() / 1000);
				appSettings.setString("stockupdatedsince", unixTime.toString());

				return value.data as Array<Car>;
			})
			.catch((err: any) => {
				console.log("Error with cars: ", err);
				alert({
					title: "Could not get vehicles from server.",
					message: err.status + " " + err.statusText,
					okButtonText: "OK"
				});
				console.log("error ::: ", err);

				return null;
			});

	}

	public async loadCarsUpdatedSince(unixTime): Promise<Array<Car>> {
		 const headers: HttpHeaders = new HttpHeaders({
			 "Content-Type": "application/json"
		 });

		 return this.http.get(Config.ApiUrl + "/api/stock/updatedsince/" + unixTime, {headers})
			 .toPromise()
			 .then((value: any) => {
				 const unixTime = Math.round((new Date()).getTime() / 1000);
				 appSettings.setString("stockupdatedsince", unixTime.toString());
				 let myArr: Array<Car>;
				 myArr = [];
				 if (value.data.length > 0) {
					 for (const car of value.data as Array<Car>) {
						 myArr.push(car);
					 }
					 if (myArr.length > 0) {
						 return myArr as Array<Car>;
					 } else {
						 return null;
					 }

				 } else {
					 return null;
				 }
			 })
			 .catch((err: any) => {
				 console.log("Error with cars");
				 alert({
					 title: "Could not get vehicles",
					 message: err.content,
					 okButtonText: "OK"
				 });
				 console.log("error ::: ", err);

				 return null;
			 });
	}

	public update(carModel: Car): void {
		const updateCar = JSON.stringify(carModel);
		fromUrl(Config.ApiUrl + "api/stock/" + carModel.id).then()
		request({
			url: Config.ApiUrl + "api/stock/" + carModel.id,
			method: "PUT",
			headers: { "Content-Type": "application/json", Authorization: "Bearer " + appSettings.getString("accessToken")},
			content: updateCar
		}).then((response: HttpResponse) => {
			if (response.statusCode === 204) {
				alert({
					title: "Vehicle successfully updated",
					message: carModel.id + " updated",
					okButtonText: "OK"
				});
			} else {
				alert({
					title: "Problem updating " + carModel.id,
					message: "failed with status code " + response.statusCode + " try again later.",
					okButtonText: "OK"
				});
			}
		}, (e) => {
			alert({
				title: "Problem updating " + carModel.id,
				message: "failed with status code " + e.toString() + " try again later.",
				okButtonText: "OK"
			});
		});
	}

	public async onImageLoaded(args: EventData, url: string): Promise<void> {
		console.log("URL : ", url);
		await fromUrl(url)
			.then((res: ImageSource) => {
				// do nothing if image loads successfully
				console.log("IMAGES LOADED...");
			}).catch((err) => {
				console.log("ERR : ", err);
				const myImg = <Image>args.object;
				myImg.src = "~/images/imagenotfound.png";
			});
	}

	private handleErrors(error: Response): Observable<never> {
		return throwError(error);
	}
}
