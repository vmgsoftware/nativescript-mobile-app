import "reflect-metadata";
import {
	Connection,
	DeleteResult,
	EntityManager,
	FindConditions,
	FindManyOptions, FindOneOptions,
	getConnection,
	getManager,
	ObjectType,
	Repository
} from "typeorm/browser";

export type OrderBy<TEntity> = { [P in keyof TEntity]?: "ASC" | "DESC" | 1 | -1 };

export class GenericRepository<TEntity> {

	private readonly targetOrEntityRegex: RegExp = /(function)*([^A-Za-z])/g;
	private  _targetOrEntity: string;

	constructor(private myObjType: ObjectType<TEntity>) {
		this.getTargetOrEntity(myObjType);
	}

	public async upsert(entity: TEntity | Array<TEntity>): Promise<void> {
		const entityManager: EntityManager = getManager();
		await entityManager.transaction(async (manager: EntityManager) => {
			await manager.save(this._targetOrEntity, entity);
		});
	}

	public async bulkUpsertById(entities: Array<TEntity>): Promise<void> {
		const databaseConnection: Connection = getConnection();
		for (const entity of entities) {

			try {
				if (entity.hasOwnProperty("id")) {

					const dbEntity: TEntity = await this.getFirstBySearch({
						where: [{ id: entity["id"] }]
					});

					if (dbEntity && dbEntity.hasOwnProperty("generatedId")) {
						entity["generatedId"] = dbEntity["generatedId"];
					}
					await databaseConnection.manager.save(this._targetOrEntity, entity);
				}
			} catch (ex) {
				console.log("ERROR UPSERTING BY ID", entity, ex);
			}

		}
	}

	public async remove(entity: TEntity): Promise<void> {
		const databaseConnection: Connection = getConnection();
		const entityRepo: Repository<TEntity> = databaseConnection.getRepository(this._targetOrEntity);
		await entityRepo.remove(entity);
	}

	public async removeAll(): Promise<void> {
		const entities: Array<TEntity> = await this.getAll();
		for (const entity of entities) {
			await this.remove(entity);
		}
	}

	public async bulkRemove(whereClause: FindConditions<TEntity>): Promise<DeleteResult> {
		const databaseConnection: Connection = getConnection();
		const entityRepo: Repository<TEntity> = databaseConnection.getRepository(this._targetOrEntity);

		return entityRepo.delete(whereClause);
	}

	public async bulkRemoveById(entities: Array<TEntity>): Promise<void> {
		const databaseConnection: Connection = getConnection();
		const entityRepo: Repository<TEntity> = databaseConnection.getRepository(this._targetOrEntity);
		for (const entity of entities) {
			try {
				if (entity.hasOwnProperty("id")) {
					const databaseEntity: TEntity = await this.getFirstBySearch({where: [{id: entity["id"]}]});

					if (databaseEntity) {
						await entityRepo.remove(databaseEntity);
					}
				}

			} catch (ex) {
				console.log("ERROR Removing BY ID", entity, ex);
			}
		}
	}

	public async getById(id: string | number): Promise<TEntity> {
		const databaseConnection: Connection = getConnection();
		const entityRepo: Repository<TEntity> = databaseConnection.getRepository(this._targetOrEntity);

		return entityRepo.findOne(id);
	}

	public async getFirstBySearch(conditions: FindOneOptions<TEntity>): Promise<TEntity> {
		const databaseConnection: Connection = getConnection();
		const entityRepo: Repository<TEntity> = databaseConnection.getRepository(this._targetOrEntity);

		return entityRepo.findOne(conditions);
	}

	public async getAllBySearch(conditions: FindManyOptions<TEntity>): Promise<Array<TEntity>> {
		const databaseConnection: Connection = getConnection();
		const entityRepo: Repository<TEntity> = databaseConnection.getRepository(this._targetOrEntity);

		return entityRepo.find(conditions);
	}

	public async getSearchCount(options?: FindManyOptions<TEntity>): Promise<[Array<TEntity>, number]> {
		const databaseConnection: Connection = getConnection();
		const entityRepo: Repository<TEntity> = databaseConnection.getRepository(this._targetOrEntity);

		return entityRepo.findAndCount(options);
	}

	public async getAll(): Promise<Array<TEntity>> {
		const databaseConnection: Connection = getConnection();
		const entityRepo: Repository<TEntity> = databaseConnection.getRepository(this._targetOrEntity);

		return entityRepo.find();
	}

	public async getTotal(options?: FindManyOptions<TEntity>): Promise<number> {
		const databaseConnection: Connection = getConnection();
		const entityRepo: Repository<TEntity> = databaseConnection.getRepository(this._targetOrEntity);

		return entityRepo.count(options);
	}

	public async getAllOrderBy(orderBy?: OrderBy<TEntity>): Promise<Array<TEntity>> {
		const databaseConnection: Connection = getConnection();
		const entityRepo: Repository<TEntity> = databaseConnection.getRepository(this._targetOrEntity);

		if (orderBy) {
			return entityRepo.find({
				order: orderBy
			});
		} else {
			return entityRepo.find({});
		}
	}

	public async getAllPaged(offset: number, limit: number, orderBy?: OrderBy<TEntity>): Promise<Array<TEntity>> {
		const databaseConnection: Connection = getConnection();
		const entityRepo: Repository<TEntity> = databaseConnection.getRepository(this._targetOrEntity);

		if (orderBy) {
			return entityRepo.find({
				skip: offset,
				take: limit,
				order: orderBy
			});
		} else {
			return entityRepo.find({
				skip: offset,
				take: limit
			});
		}
	}

	private getTargetOrEntity(objType: ObjectType<TEntity>): void {
		 this._targetOrEntity = objType.toString().replace(this.targetOrEntityRegex, "");
	}

}
