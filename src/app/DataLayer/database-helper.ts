import { Injectable } from "@angular/core";
import { Connection, ConnectionManager, createConnection, getConnection, getConnectionManager } from "typeorm/browser";
import { Config } from "~/app/Config";

@Injectable({
	providedIn: "root"
})
export class DatabaseHelperService {

	public async clearDatabase(): Promise<void> {
		const dbConnection: Connection = await getConnection();
		await dbConnection.synchronize(true);
	}

	public async createDbConnection(): Promise<Connection> {
		const connectionManager: ConnectionManager = getConnectionManager();
		if (connectionManager.connections != null && connectionManager.connections.length > 0) {
			return;
		}

		const driver = require("nativescript-sqlite");

		return createConnection({
			name: "default",
			type: "nativescript",
			driver,
			database: Config.DatabaseName,
			entities: Config.Entities
		}).then(async (connection) => {
			await connection.synchronize(false);

			return connection;
		}).catch((error) => {
			return error;
		});

	}

}
