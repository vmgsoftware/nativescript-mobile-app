import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm/browser";

@Entity()
export class  MMCode {

	@PrimaryGeneratedColumn()
	public id: number;

	@Column()
	public mmcode: string;

	@Column()
	public make: string;

	@Column()
	public model: string;

	@Column({
		nullable: true,
		type: "text"
	})
	public variant: string;

	@Column({
		nullable: true,
		type: "text"
	})
	public companyBranchGuid: string;

	@Column({
		nullable: true,
		type: "text"
	})
	public company: string;

	@Column()
	public dateCreated: string;

	@Column()
	public dateUpdated: string;

	   @Column({nullable: true, type: "text"})
	public years: string;

	   @Column({
		nullable: true,
		type: "text"
	})
	public category: string;

	   @Column({nullable: true})
	public masterModel: string;

}
