import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm/browser";
import { Car } from "~/app/DataLayer/Entities/Car";

@Entity()
export class CarImage {
	@PrimaryGeneratedColumn()
	id: number;
	@Column({nullable: true})
	imageUrl: string;
	@ManyToOne((type) => Car, (car) => car.imageUrls,  {onDelete: "CASCADE"})
	car: Car;
}
