import { Column, Entity, OneToMany, PrimaryColumn } from "typeorm/browser";
import { CarImage } from "~/app/DataLayer/Entities/CarImage";
@Entity()
export class Car {

	@PrimaryColumn()
	public id: string;
	@Column({nullable: true})
	public dmsStockId: number;
	@Column({nullable: true})
	public stockCode: string;
	@Column({nullable: true})
	public mmCode: string;
	@Column({nullable: true})
	public make: string;
	@Column({nullable: true})
	public category: string;
	@Column({nullable: true})
	public model: string;
	@Column({nullable: true})
	public masterModel: string;
	@Column({nullable: true})
	public variant: string;
	@Column({nullable: true})
	public year: number;
	@Column({type: "int", nullable: true})
	public mileage: number;
	@Column({nullable: true})
	public colour: string;
	@Column({nullable: true })
	public condition: string;
	@Column({type: "decimal", nullable: true})
	public purchasePrice: number;
	@Column({type: "decimal", nullable: true})
	public sellingPrice: number;
	@OneToMany((type) => CarImage, (image) => image.car, {eager: true, onDelete: "CASCADE", cascade: true})
	public carImages: Array<CarImage>;
	@Column({nullable: true})
	public branchGuid: string;
	@Column({nullable: true})
	public dateCreated: string;
	@Column({nullable: true})
	public dateUpdated: string;
	@Column({nullable: true})
	public deleted: boolean;
	@Column({nullable: true})
	public vin: string;
	@Column({nullable: true})
	public km: string;

	private _images: Array<string>;

	get imageUrls(): Array<string> {
		if (this.carImages) {
			this.carImages.map((img) => img.imageUrl);
			if (this.carImages.map((img) => img.imageUrl).length > 0) {
				return this.carImages.map((img) => img.imageUrl);
			} else {
				const myAr = ["~/images/imagenotfound.png"];
				return myAr;
			}
		}

	}

	get thumbNail(): string {
		if (this.carImages) {
			this.carImages.map((img) => img.imageUrl);
			if (this.carImages.map((img) => img.imageUrl).length > 0) {
				const thumb = this.carImages.map((img) => img.imageUrl);
				return thumb[0];
			} else {
				return "~/images/imagenotfound.png";
			}
		}
	}
}
