import { Column, Entity, PrimaryGeneratedColumn } from "typeorm/browser";

@Entity()
export class Lead {
	@PrimaryGeneratedColumn()
	generatedId: number;
	@Column({nullable: true})
	id: string;
	@Column({nullable: true})
	dmsLeadId: string;
	@Column({nullable: true})
	mobileStockId: string;
	@Column({nullable: true})
	userId: string;
	@Column({nullable: true})
	branchGuid: string;
	@Column({nullable: true})
	firstname: string;
	@Column({nullable: true})
	lastname: string;
	@Column({nullable: true})
	emailAddress: string;
	@Column({nullable: true})
	cellphoneNumber: string;
	@Column({nullable: true})
	addressLine1: string;
	@Column({nullable: true})
	addressLine2: string;
	@Column({nullable: true})
	addressLine3: string;
	@Column({nullable: true})
	region: string;
	@Column({nullable: true})
	make: string;
	@Column({nullable: true})
	model: string;
	@Column({nullable: true})
	variant: string;
	@Column({nullable: true})
	year: number;
	@Column({nullable: true})
	price: number;
	@Column({nullable: true})
	colour: string;
	@Column({nullable: true})
	dateCreated: string;
	@Column({nullable: true})
	dateUpdated: string;
	@Column({nullable: true})
	isOnline: boolean;
	@Column({nullable: true})
	deleted: boolean;

}
