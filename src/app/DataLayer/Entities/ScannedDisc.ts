import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm/browser";

@Entity()
export class ScannedDisc {
	@PrimaryGeneratedColumn()
	public id: number;
	@Column()
	public make: string;
	@Column()
	public model: string;
	@Column({nullable: true})
	public variant: string;
	@Column()
	public description: string;
	@Column()
	public vin: string;
	@Column()
	public registrationNumber: string;
	@Column({nullable: true})
	public licenseNumber: string;
	@Column()
	public engineNumber: string;
	@Column({nullable: true})
	public colour: string;
	@Column({nullable: true})
	public condition: string;
	@Column()
	public dateScanned: string;
	@Column()
	public recordLive: boolean;
	@Column()
	public status: string;
	@Column({nullable: true})
	public year: number;
	@Column({nullable: true})
	public tradePrice: number;
	@Column({nullable: true})
	public retailPrice: number;
	@Column({nullable: true})
	public mmCode: string;
	@Column({nullable: true})
	public mileage: number;
	@Column({nullable: true})
	public introDate: number;

	public GetJson() {
		const myJSON = {
			make: this.make,
			model: this.model,
			description: this.description,
			vin: this.vin,
			registrationNumber: this.registrationNumber,
			licenseNumber: this.licenseNumber,
			engineNumber: this.engineNumber,
			colour: this.colour,
			condition: this.condition,
			dateScanned: this.dateScanned,
			status: this.status
		};

		return JSON.stringify(myJSON);
	}

}
